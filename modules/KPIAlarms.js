/**
 * Created by Radhika on 7/11/2017.
 */

var moment = require('moment')
var redis = require('redis');
  var redisSubmit = redis.createClient(6379, '172.16.23.27');

exports.gemsActions = function gemsActions (req, res, next){
	res.send("Received Successfully");
}

function kpiAlarms(req, res, next) {
    var jsonObj = JSON.parse(req.body.message)
    logger.info(" In kpi the parsed message is" + JSON.stringify(jsonObj))
    jsonObj.time = moment.parseZone(req.body.time).utc().format("YYYY-MM-DD HH:mm:ss");
    jsonObj.level = req.body.level
	redisSubmit.HINCRBY(jsonObj.TYPE+'_'+jsonObj.PRODUCT_NAME, jsonObj.level, 1, function (err, resp) {
        if (err) {
            logger.error("Got error while incrementing Redis Hash" + err);
        }
    });
	redisSubmit.HINCRBY(jsonObj.TYPE+'_'+jsonObj.PRODUCT_NAME, jsonObj.previousLevel, -1 ,function(errWhileDecrementing,respAfterDecrement){
    if(errWhileDecrementing){
           logger.error("Got error while decrementing Redis Hash" + err);
    	}
    });
	redisSubmit.HGETALL(jsonObj.TYPE + '_' + jsonObj.PRODUCT_NAME, function (err, resp) {
        if (err) {
            logger.error(err);
        } else {
            var tileJson = {
                "product": jsonObj.PRODUCT_NAME,
                "key": jsonObj.TYPE,
                '_id': jsonObj.PRODUCT_NAME + jsonObj.TYPE,
                "majorIssues": resp.WARNING,
                "criticalIssues": resp.CRITICAL
            };
            //logger.info("Data in HGETALL : " + JSON.stringify(tileJson));
            io.to(jsonObj.PRODUCT_NAME).emit("message_from_subscribed", tileJson);
			logger.info("All is well with elastic search")
			elasticClient.index({
				index: 'kpi_alarms',
				type: 'logs',
				body: jsonObj
			}, function (error, response) {
				if (error) {
					logger.error("error with pushing data to elastic search " + error)
					return next(error)

				}
				logger.info("the response from elastic search index query is " + JSON.stringify(response));
				jsonObj._id = response['_id']
				if(jsonObj.level == "CRITICAL")
				{
					io.to(jsonObj.PRODUCT_NAME).emit("alertsWindow", jsonObj);
				}
			});
		}
	})
	res.send("KPI alarm sent successfully");
}


function search(ID, index, type, callback) {
    elasticClient.search({
        index: index,
        type: type,
        body: {
            query: {
                match: {
                    _id: ID
                }
            }
        }
    }, function (err, data) {
        if (err) {
            var jsonObj = {"success": "0"}
            return callback(null, jsonObj)


        }
        if (data.hits.total > 0) {
            var jsonObj = {"success": "1", "document": data}
            return callback(null, jsonObj)
        }
        else {
            var jsonObj = {"success": "0"}
            return callback(null, jsonObj)
        }
    })
}


function updateKPIAlarms(nextDocumentID, ID, index, type, alarm, status, callback) {
    logger.info("update KPI_ALARMS index with the ID:" + ID + " " + "index:" + index + " " + "alarm status:" + status + " " + "alarm time" + alarm)
    elasticClient.update({
        index: index,
        type: type,
        id: ID,
        body: {
            // put the partial document under the `doc` key
            doc: {
                ALARM_TIME: alarm,
                ALARM_STATUS: status,
                next_doc_id: nextDocumentID
            }
        }
    }, function (error, response) {
        if (error) {
            return callback(error)
        }
        return callback(null, response)
    })

}


function updateDocumentWithIPAlarmNameAsIndex(ID, index, type, obj, callback) {
    delete obj._id
    delete obj.previousLevel
    var index = 'alarms' // check
    logger.info("update documentWithIPAlarmNameAsIndex with the index:" + index + "" + " " + "with the ID:" + ID + " " + "type:" + type + " " + "body:" + JSON.stringify(obj))
    elasticClient.index({
        index: index,
        type: type,
        id: ID,
        body: obj
    }, function (err, data) {
        if (err) {
            return callback(err)
        }
        return callback(null, data)
    })
}


exports.kpi_alarms = kpiAlarms
