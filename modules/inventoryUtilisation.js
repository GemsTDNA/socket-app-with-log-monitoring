var Influx = require('influx');
var influx = new Influx.InfluxDB({
    host: '10.225.253.132',
    database: 'netdata'
});
var moment = require('moment');
var sprintf = require("sprintf-js").sprintf,
    vsprintf = require("sprintf-js").vsprintf;

/*exports.sendInventoryUtilisation = function sendInventoryUtilisation(check) {
	if (check == "True") {
		setTimeout(sendInventoryUtilisation, 30000); //5 minutes
	}
	var query = "select * from Avg_TPS ORDER BY time DESC limit 4";
	logger.info("Query : " + query);
	influx.query(query).then(function (result) {
		var resArray = [];
		result.forEach(function (eachResult) {
			eachResult.time = moment.parseZone(eachResult.time).local().format("YYYY-MM-DD HH:mm:ss");
			eachResult.USED = parseInt(sprintf("%0.0f", eachResult.TPS));
			eachResult.PERCENT = parseInt(sprintf("%0.0f", eachResult.TPS_Usage));
			delete eachResult.TPS_Usage;
			eachResult.AVAILABLE = (1000 - parseInt(sprintf("%0.0f", eachResult.TPS)));
			eachResult.MEASUREMENT = eachResult.Measurement;
			eachResult.LICENSED = 1000;
			delete eachResult.TPS;
			delete eachResult.Measurement
			resArray.push(eachResult);
		});
		 resArray[0].INVENTORY_NAME = "SMPP-IRCTC";
			resArray[1].INVENTORY_NAME = "SMPP-CITIBANK";
			resArray[1].LICENSED=1000;
			resArray[1].PERCENT=95;
			resArray[1].USED = 950;
			resArray[1].AVAILABLE = 50;
			resArray[2].INVENTORY_NAME = "SMPP-PNB";
		logger.info("Message for InventoryUtilisation : " + JSON.stringify(resArray));
		io.to("SMSC").emit("InventoryUtilisation", resArray);
	}).catch(function (err) {
		logger.error(err);
	});
}*/

exports.sendInventoryUtilisation = function (req, res) {
	var query = "select * from Avg_TPS ORDER BY time DESC limit 4";
	logger.info("Query : " + query);
	influx.query(query).then(function (result) {
		var resArray = [];
		result.forEach(function (eachResult) {
			logger.silly(JSON.stringify(eachResult));
			eachResult.INVENTORY_NAME =  eachResult.PRODUCT_NAME;
			eachResult.time = moment.parseZone(eachResult.time).local().format("YYYY-MM-DD HH:mm:ss");
			eachResult.USED = parseInt(sprintf("%0.0f", eachResult.TPS));
			eachResult.PERCENT = parseInt(sprintf("%0.0f", eachResult.TPS_Usage));
			delete eachResult.TPS_Usage;
			eachResult.AVAILABLE = (1000 - parseInt(sprintf("%0.0f", eachResult.TPS)));
			eachResult.MEASUREMENT = eachResult.Measurement;
			eachResult.LICENSED = 1000;
			delete eachResult.TPS;
			delete eachResult.Measurement
			resArray.push(eachResult);
		});
		 /*resArray[0].INVENTORY_NAME = "SMPP-IRCTC";
			resArray[1].INVENTORY_NAME = "SMPP-CITIBANK";
			resArray[2].INVENTORY_NAME = "SMPP-PNB";*/
			resArray[2].LICENSED=1000;
                        resArray[2].PERCENT=10;
                        resArray[2].USED = 102;
                        resArray[2].AVAILABLE = 898;
			resArray[1].LICENSED=1000;
                        resArray[1].PERCENT=95;
                        resArray[1].USED = 950;
                        resArray[1].AVAILABLE = 50;
			resArray[1].LICENSED=1000;
			resArray[1].PERCENT=95;
			resArray[1].USED = 950;
			resArray[1].AVAILABLE = 50;
		logger.info("Message for InventoryUtilisation : " + JSON.stringify(resArray));
		//io.to("SMSC").emit("InventoryUtilisation", resArray);
		res.send(resArray);
	}).catch(function (err) {
		logger.error(err);
	});
}
