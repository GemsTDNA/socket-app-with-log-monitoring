/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

const winston = require('winston');
var kafka = require('kafka-node')
const fs = require('fs');
var elasticsearch = require('elasticsearch');
elasticClient = new elasticsearch.Client({
    host: '172.16.23.14:9200',
    log: 'info'
});

const env = 'development';
const logDir = 'log';
const tsFormat = () => (new Date()).toLocaleTimeString();
if (!fs.existsSync(logDir)) {
    fs.mkdirSync(logDir);
}
const logger = new (winston.Logger)({
    transports: [
        // colorize the output to the console
        new (winston.transports.Console)({
            timestamp: tsFormat,
            colorize: true,
            level: 'info'
        }),
        new (require('winston-daily-rotate-file'))({
            filename: `${logDir}/-local-Kafka_Consumer_debug.log`,
            timestamp: tsFormat,
            datePattern: 'yyyy-MM-dd',
            prepend: true,
            level: env === 'development' ? 'verbose' : 'info'
        })
    ]
});
var ConsumerGroup = kafka.ConsumerGroup;

var consumerOptions = {
    host: '172.16.23.20:2181',
    groupId: 'MonitoringCounters',
    sessionTimeout: 15000,
    protocol: ['roundrobin'],
    autoCommit: true,
    //fromOffset: 'earliest', // equivalent of auto.offset.reset valid values are 'none', 'latest', 'earliest'
    fromOffset: 'latest', // equivalent of auto.offset.reset valid values are 'none', 'latest', 'earliest'
    fetchMaxWaitMs: 1000,
    fetchMaxBytes: 1024 * 1024
};

logger.info('consumer groups options');
logger.info(consumerOptions);


//var topics = ['Demo'];

var consumerGroup = new ConsumerGroup(consumerOptions, 'test');
consumerGroup.on('error', onError);
//consumerGroup.on('message', onMessage);

consumerGroup.on('message', function (message) {
    logger.info(message)
    var displayName = 'testing4'
    var obj = {
        42: 100,
        43: 56,
        55: 67
    }
    elasticClient.search({
        index: "log_monitoring_counters", //fetch from log_monitoring_http
        type: "logs",
        body: {
            size: 1000,
            query: {
                displayName: displayName
            }
        }
    }, function (err, data) {
        if (err) {
            logger.error("err fetching data from log_monitoring_counters:" + JSON.stringify(err))
        }
        var comparePattern = data.hits.hits[0]._source.comparePattern  //"43!=56&&55==67"
        //comparePattern.replace("Microsoft", "W3Schools");
        comparePattern.forEach(function(eachCharacter){
            console.log(eachCharacter)
        })




        res.json({"status": 1, "data": array});
    })


    //check if 45>55 and 23>56 is in elastic search

    // if yes, insert to alarms
    // if no , do nothing
    //console.log(message);
});

function onError(error) {
    logger.error(error);
    logger.error(error.stack);
    console.log(error)
}


//function onMessage(message) {
//    logger.verbose('%s read msg Topic="%s" Partition=%s Offset=%d', this.client.clientId, message.topic, message.partition, message.offset);
// // do something with this data !!
// console.log("message recieved!!")
// 
//}

