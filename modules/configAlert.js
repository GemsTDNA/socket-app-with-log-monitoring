exports.configAlerts = function (req, res) {
	logger.silly("In the function of config alert");
	logger.silly(JSON.stringify(req.body));
        elasticClient.search({
			index: 'configdiff',
			type: 'refer',
			body: {
			  "query": {
				"bool": {"must": [{"term": {"PRODUCT_NAME": req.body.PRODUCT_NAME}},
					{"term": {"VENDOR_NAME": req.body.VENDOR_NAME}},
					{"term": {"IP_ADDRESS": req.body.IP_ADDRESS}},
					{"term": {"fileName": req.body.fileName}}]
				}
			  }
			}
	}, function (err, response) {
		if (err) {
			logger.error(err);
		} else {
			logger.silly(JSON.stringify(response));
			var jsonObj = response.hits.hits[0]['_source'];
			
			req.body['Original'] = jsonObj['Running_file'];
			jsonObj['Running_file'] = req.body.modified;
			req.body['Modified'] = req.body.modified;
			logger.silly(JSON.stringify(jsonObj));
			elasticClient.index({
				index: 'configdiff',
				type: 'refer',
				id: response.hits.hits[0]['_id'],
				body: jsonObj
			}, function (err, response) {
				logger.silly(response);
				elasticClient.index({
					index: 'configdiff',
					type: 'logs',
					body: req.body
				}, function (err, response) {
					if (err) {
						logger.error(err)
					} else {
						req.body['_id'] = response['_id'];
						io.to("SMSC").emit("configtrail", req.body);
						res.send("Ok");        	
					}
				});
			})
		}
	});
	
}
