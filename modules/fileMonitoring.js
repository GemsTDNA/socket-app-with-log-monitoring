/**
 * Created by Radhika on 9/29/2017.
 */
var config = require('config')
var redis = require('redis');
var redisClient = redis.createClient(config.redis.port, config.redis.host);
var redisSubmit = redis.createClient(config.redis.port, config.redis.host);
var request = require('request')
var moment = require('moment')
var MongoClient = require('mongodb').MongoClient
var fs = require('fs');
var client = require('scp2');
var path = require('path')
var url = config.monitoring.mongoURL//"mongodb://172.16.23.20:27017/agenda";
var util = require('util')

var errorHandler = require(config.errorMessages.path)
var fileLogger = require('../libs/logger').fileLogger;
var HTTPLogger = require('../libs/logger').HTTPLogger
var counterLogger = require('../libs/logger').counterLogger
var logger = require('../libs/logger').logger


function addLogMonitoringData(req, res, next) {


    fileLogger.info(req.body)
    fileLogger.info("addLogMonitoring Function called:" + JSON.stringify(req.body))
    fileLogger.info("patternArray is:" + JSON.stringify(req.body.patternArray))
    fileLogger.info(req.body.patternArray)


    var fileName = req.body.fileName
    var path = req.body.path
    var watchDir = req.body.watchDirectory
    var pattern = req.body.pattern // array
    var IPAdress = req.body.IP
    var PRODUCT_NAME = req.body.product
    var VENDOR_NAME = req.body.vendor
    var LOCATION = req.body.location
    var displayName = req.body.displayName


    //validation
    validateWhenAddingLogMonitoringData(PRODUCT_NAME, LOCATION, VENDOR_NAME, IPAdress, pattern, path, displayName, 'log_monitoring', 'logs', function (validationErr, validated) {
        if (validationErr) {
            return next(validationErr)
        }


        //add this to elastic search


        if (watchDir == true) { //is a directory , provide extension
            var extension = filename.split('.')[1]
            req.body.extension = extension
            var jsonObj = {
                //  "fileName": fileName,
                "path": path,
                "IP_ADDRESS": IPAdress,
                "pattern": req.body.pattern,
                "caseSensitive": req.body.caseSensitive,
                "TYPE": req.body.type,
                "watchDirectory": req.body.watchDirectory,
                "riseAlarm": req.body.riseAlarm,
                "extension": extension,
                "processing": 1,
                "PRODUCT_NAME": PRODUCT_NAME,
                "LOCATION": LOCATION,
                "VENDOR_NAME": VENDOR_NAME

            }

        } else {
            var jsonObj = {
                "fileName": fileName,
                "path": path,
                "IP_ADDRESS": IPAdress,
                "pattern": req.body.pattern,
                "caseSensitive": req.body.caseSensitive,
                "TYPE": req.body.type,
                "watchDirectory": req.body.watchDirectory,
                "riseAlarm": req.body.riseAlarm,
                "processing": 1,
                "PRODUCT_NAME": PRODUCT_NAME,
                "LOCATION": LOCATION,
                "VENDOR_NAME": VENDOR_NAME

            }
        }
        fileLogger.info("watchDir has value:" + watchDir)
        fileLogger.info("the json obj emitted is:" + JSON.stringify(jsonObj))
        var arr = []
        var sessionid
        var stopLoop = false
        fileLogger.info("calling getNumberOfRecords Stored with the IP:" + req.body.IP)


        getNumberOfRecordsStored(req.body.IP, function (errGettingNumberOfRecords, recordsFound) {
            if (errGettingNumberOfRecords) {
//            logger.error("error from function getNumberOfRecordsStored:" + errGettingNumberOfRecords)
//            fileLogger.error("error from function getNumberOfRecordsStored:" + errGettingNumberOfRecords)
                return next(errGettingNumberOfRecords)
            }
            fileLogger.info("elasticSearch query to check if IP:" + req.body.IP + " exists")
            fileLogger.info("recordsFound is:" + JSON.stringify(recordsFound))
            var jsonObjInEs = {
                "fileName": fileName,
                "path": path,
                "IP_ADDRESS": IPAdress,
                "pattern": req.body.pattern,
                "caseSensitive": req.body.caseSensitive,
                "TYPE": req.body.type,
                "watchDirectory": req.body.watchDirectory,
                "riseAlarm": req.body.riseAlarm,
                "processing": 1,
                "severity": req.body.severity,
                "PRODUCT_NAME": PRODUCT_NAME,
                "LOCATION": LOCATION,
                "VENDOR_NAME": VENDOR_NAME,
                "displayName": req.body.displayName,
                "patternArray": req.body.patternArray,
                "patternPreview": req.body.patternPreview

            }
            fileLogger.info("inserting the following data into ES on addMonitoring")
            fileLogger.info(jsonObjInEs)
            insertMonitoringData("log_monitoring", "logs", jsonObjInEs, function (err, response) {
                if (err) {
                    fileLogger.error("error inserting data to elastic search:" + JSON.stringify(err))
                    return next(err)
                }
                var IDOfRecordInserted = response._id
                var IPOfRecordInserted = req.body.IP
                if (recordsFound.count == 0) {
                    // use salt command to start  log Monitoring which is a nodejs process
                    fileLogger.info('zero records found')
                    fileLogger.info('use salt command to start  log Monitoring which is a nodejs process')
                    saltMinionCommand(IPOfRecordInserted, function (errStartingSaltProcess, processStarted) {
                        if (errStartingSaltProcess) {
                            // logger.info("the salt Minion throws the following err")
                            fileLogger.error(errStartingSaltProcess)
                            return next(errStartingSaltProcess)
                        }

                        fileLogger.info("salt process started" + JSON.stringify(processStarted))

                    })
                }
                //check if key  exists in redis
                // yes - add or append or change status
                // no - create
                determineIfkeyExistsInRedis(IPOfRecordInserted, function (errWhileCheckingIfKeyExists, keyInformation) {
                    if (errWhileCheckingIfKeyExists) {
                        //logger.error("err in function determineIfkeyExistsInRedis:" + JSON.stringify(errWhileCheckingIfKeyExists))
                        return next(errWhileCheckingIfKeyExists)
                    }
                    fileLogger.info("the key is:" + IPOfRecordInserted)
                    fileLogger.info("the key info is" + keyInformation)
                    if (keyInformation == 0) {
                        //create redis keys
                        fileLogger.info("redis hash with the key:" + IPOfRecordInserted + " " + "doesn't exist.")
                        res.send({"status": 1})
                        fileLogger.info("creating redis hash:" + IPOfRecordInserted)
                        createRedisHash(IPOfRecordInserted, function (errWhileCreatingRedisHash, redisHashCreated) {
                            if (errWhileCreatingRedisHash) {
                                fileLogger.error("err in function createRedisHash:" + JSON.stringify(errWhileCreatingRedisHash))
                                return next(redisHashCreated)

                            }
                            fileLogger.info("creating redis set:" + IPOfRecordInserted)
                            createRedisSet(IPOfRecordInserted, IDOfRecordInserted, function (errWhileCreatingRedisSet, redisSetCreated) {
                                if (errWhileCreatingRedisSet) {
                                    fileLogger.error("err in function createRedisSet:" + JSON.stringify(errWhileCreatingRedisSet))
                                    return next(errWhileCreatingRedisSet)
                                }
                                fileLogger.info(redisSetCreated)

                            })
                        })
                    } else {
                        res.send({"status": 1})
                        var stopLoop = false
                        var sessionid

                        createRedisSet(IPOfRecordInserted, IDOfRecordInserted, function (errWhileCreatingRedisSet, redisSetCreated) {
                            if (errWhileCreatingRedisSet) {
                                fileLogger.error("err in function createRedisSet:" + JSON.stringify(errWhileCreatingRedisSet))
                                return next(errWhileCreatingRedisSet)

                            }

                            fileLogger.info("redisSetCreated for the IP:" + IPOfRecordInserted + " " + redisSetCreated)
                            fetchHashes(IPOfRecordInserted, function (errObtainingStatus, statusFound) {
                                if (errObtainingStatus) {
                                    fileLogger.error("err from function fetchHashes:" + JSON.stringify(errObtainingStatus))
                                    return next(errObtainingStatus)
                                }
                                if (statusFound == 'connected') {
                                    fileLogger.info("the status in the redis hash is connected")
                                    sendAllData('log_monitoring', 'logs', function (errOnReconnectStatus, allESData) {
                                        if (errOnReconnectStatus) {
                                            return callback(errOnReconnectStatus)
                                        }
                                        fileLogger.info('emitting the following data on connect:' + JSON.stringify(allESData))
                                        if (allESData.length > 0) {
                                            allESData.forEach(function (itemToEmit) {
                                                io.to(sessionid).emit('startProcessing', itemToEmit)
                                            })
                                        }


                                        IPAndSocketSession.forEach(function (eachObj) {
                                            if (stopLoop) {
                                                return
                                            }
                                            if (eachObj[IPOfRecordInserted]) {
                                                sessionid = eachObj[IPOfRecordInserted]
                                                stopLoop = true;
                                                //
                                            }
                                            jsonObj.id = response._id
                                            fileLogger.info("emitting the following data to startProcessing event")
                                            fileLogger.info(jsonObj)
                                            io.to(sessionid).emit("startProcessing", jsonObj)


                                        })
                                    })


                                } else {
                                    fileLogger.info("status is not connected:" + statusFound)
                                }
                            })

                        })

                    }
                })

            })
        })
    })// end of validation

}


function createRedisHash(IP, callback) {
    redisSubmit.hset(IP, "status", "awaitingConnection", function (err, res) {
        if (err) {
            return callback(err)
        }
        //redisSubmit.expire(IP,300)
        return callback(null, res)
    });
}

function createRedisSet(IP, ID, callback) {
    //logger.info("create redis set called with params " + IP + " " + ID)
    redisSubmit.SADD(IP + "_set", ID, function (err, res) {
        if (err) {
            return callback(err)
        }
        redisSubmit.expire(IP + "_set", 300)
        return callback(null, res)
    })
}

function determineIfkeyExistsInRedis(IP, callback) {

    redisSubmit.exists(IP, function (err, res) {
        if (err) {
            return callback(err)
        }
        return callback(null, res)
    })
}

//not used
function fetchAllElementsOfset(IP, callback) {
    redisSubmit.smembers(IP + "_set", function (err, res) {
        if (err) {
            return callback(err)
        }
        return callback(null, res)
    })
}

function fetchHashes(IP, callback) {
    redisSubmit.hget(IP, "status", function (err, res) {
        if (err) {
            return callback(err)
        }
        return callback(null, res)
    })
}


function getNumberOfRecordsStored(IP, callback) {
    elasticClient.count({
        index: "log_monitoring",
        type: "logs",
        body: {
            query: {
                match: {"IP_ADDRESS": IP}
            }
        }

    }, function (err, response) {
        if (err) {
            return callback(err)
        }
        return callback(null, response)
    })
}


function insertMonitoringData(index, type, data, callback) {
    // logger.info("insertMonitoringData gets the following data:"+JSON.stringify(data))
    // logger.info(data)
    elasticClient.index({
        index: index, //"log_monitoring"
        type: type, //logs
        body: data
    }, function (err, response) {
        if (err) {
            return callback(err)
        }
        return callback(null, response)
    })
}

function fetchDataBasedOnID(index, type, _id, callback) {
    elasticClient.search({
        index: index, //"log_monitoring"
        type: type, //logs
        body: {
            query: {
                match: {_id: _id}
            }
        }
    }, function (err, data) {
        if (err) {
            return callback(err)
        }
        return callback(null, data)
    })
}


function listData(req, res, next) {
    //list all data inserted into elastic search
    logger.info("listData function called")
    elasticClient.search({
        index: "log_monitoring,log_monitoring_http,log_monitoring_counters", //fetch from log_monitoring_http
        type: "logs",
        body: {
            size: 1000,
            query: {
                match_all: {}
            }
        }
    }, function (err, data) {
        if (err) {
            logger.error("err fetching data from log_monitoring index:" + JSON.stringify(err))
            return next(err)
        }
        var array = [];
        data.hits.hits.forEach(function (hit) {
            var jsonObj = hit['_source'];
            jsonObj['_id'] = hit['_id'];
            array.push(jsonObj);

        })
        res.json({"status": 1, "data": array});
    })


}


function getMonitoringAlarms(req, res, next) {
    //list all data inserted into elastic search
    logger.info("getMonitoringAlarms function called")
    var userName = req.body.userName
    // the below function is used when the indices and logs are diffeent
//    elasticClient.msearch({
//
//        body: [
//            {index: 'log_monitoring_alarms', type: 'logs'},
//            {query: {match_all: {}}, "size": 100, "sort": {TIME: {order: "desc"}}},
//            {index: 'log_monitoring_http', type: 'alarms'},
//            {query: {match_all: {}}, "size": 100, "sort": {TIME: {order: "desc"}}},
//            {index: 'log_monitoring_counters', type: 'alarms'},
//            {query: {match_all: {}}, "size": 100, "sort": {TIME: {order: "desc"}}}
//
//        ]
//    }, function (err, data) {
//        if (err) {
//            logger.error(err)
//        }
//        //res.send(data)
//        var array = [];
//        data.responses.forEach(function (eachObj) {
//            eachObj.hits.hits.forEach(function (obj) {
//                var jsonObj = obj['_source'];
//                jsonObj['_id'] = obj['_id'];
//                array.push(jsonObj);
//            })
//
//
//        })
//        res.json({"status": 1, "data": array});
//    })

request.post('http://172.16.23.27:3001/getProducts', {json: {UserName: userName}}, function (error, response, body) {
        if (error) {
            logger.error(error)
            return next(error)
        }
           elasticClient.search({
        index: "log_monitoring,log_monitoring_http,log_monitoring_counters",
        type: "alarms",
        body: {
            sort: {TIME: {order: "desc"}},
            size: 1000,
            query: {
                
                bool:{
                    must:{
                        terms:{"PRODUCT_NAME":body}
                    }
                }
            }
        }
    }, function (err, data) {
        if (err) {
            logger.error(err)
            return next(err)
        }

        var array = [];
        data.hits.hits.forEach(function (hit) {
            var jsonObj = hit['_source'];
            jsonObj['_id'] = hit['_id'];
            array.push(jsonObj);

        })
        res.json({"status": 1, "data": array});
    })
        })
 


}


function editPattern(req, res, next) {
    /* modify elastic search's pattern (replace)
     api called
     if status is connected or reconnected , then emit add Pattern data
     if no status found , then start salt process
     store update data in an data structure
     on connect => emit all the data

     */
    fileLogger.info("addPattern function called")

    var pattern = req.body.pattern //must be an array
    var id = req.body._id
    var IP = req.body.IP_ADDRESS
    var patternArray = req.body.patternArray
    var patternPreview = req.body.patternPreview
    fileLogger.info("pattern:" + pattern + " ," + "id:" + id + " ," + "IP:" + IP + " ," + "patternArray:" + patternArray + " ," + "pattern:" + pattern + " ," + "patternPreview:" + patternPreview + " ")
    var sessionid
    var stopLoop = false
    elasticClient.index({
        index: "log_monitoring",
        type: "logs",
        id: req.body._id,
        body: {
            PRODUCT_NAME: req.body.product,
            LOCATION: req.body.location,
            VENDOR_NAME: req.body.vendor,
            displayName: req.body.displayName,
            watchDirectory: req.body.watchDirectory,
            processing: req.body.processing,
            path: req.body.path,
            fileName: req.body.fileName,
            IP_ADDRESS: req.body.IP,
            TYPE: req.body.type,
            caseSensitive: req.body.caseSensitive,
            riseAlarm: true,
            severity: req.body.severity,
            pattern: pattern,
            patternArray: patternArray,
            patternPreview: patternPreview
        }
    }, function (err, response) {
        if (err) {
            fileLogger.error(err)
            return next(err)
        }
        //in production
        fileLogger.info("the IPAndSocketSession array is shown below")
        fileLogger.info(IPAndSocketSession)
        IPAndSocketSession.forEach(function (eachObj) {
            if (stopLoop) {
                return
            }
            if (eachObj[IP]) {
                sessionid = eachObj[IP]
                stopLoop = true;
                //
            }
        })


        fileLogger.info("the client session id  obtained from IPandSocketSession array is " + sessionid)
        if (sessionid == undefined) {
            fileLogger.info("no session ID found for IP" + IP + " " + "no workflow initiated")
        }

        workFlowForAddPattern(sessionid, IP, req.body._id, pattern, function (workFlowErr, workFlow) {
            if (workFlowErr) {
                fileLogger.error(workFlowErr)
                return next(workFlowErr)
            }
        })
        res.send({"status": 1, "data": "success"})
    })

}


//used only for testing
function getIPAddress(_id, callback) {
    elasticClient.search({
        index: 'log_monitoring',
        type: 'logs',
        body: {
            size: 1000,
            query: {
                match: {_id: _id}
            }
        }
    }, function (err, data) {
        if (err) {
            logger.error("error querying elasticSearch" + JSON.stringify(err))
            return callback(err)
        }
        if (data) {
            logger.info("elastic search returns results")
            return callback(null, data.hits.hits[0]._source.IP_ADDRESS)
            // res.json({"status":1,"data":data.hits.hits})
        }
    })
}


function stopWatchingFile(req, res, next) {
    fileLogger.info("stopWatchingFile function called:" + JSON.stringify(req.body))
    var id = req.body._id
    var IP = req.body.IP_ADDRESS
    var stopLoop = false
    var sessionid
    IPAndSocketSession.forEach(function (eachObj) {
        if (stopLoop) {
            return
        }
        if (eachObj[IP]) {
            sessionid = eachObj[IP]
            stopLoop = true;
            //
        }
    })

    fileLogger.info("the IP is" + IP)
    fileLogger.info("the socket id is:" + sessionid)
    if (sessionid == undefined) {
        fileLogger.info("the connection process is not yet established , hence socketID of client is undefined")
    }
    workFlowForStopProcessing(sessionid, IP, req.body._id, function (workFlowErr, workFlowData) {
        if (workFlowErr) {
            fileLogger.error(err)
            return next(err)
        }
    })


    deleteDataFromMonitoring(id, 'log_monitoring', 'logs', function (err, data) {
        if (err) {
            fileLogger.error(err)
            return next(err)
        }
        res.json(data)
    })

    //io.to(sessionid).emit("stopProcessing", req.body._id);

}


function workFlowForStopProcessing(sessionid, IP, ID, callback) { //ID represents elastic search _id\
    fileLogger.info('workflowForstopProcessing called with the IP' + IP)
    getNumberOfRecordsStored(IP, function (errGettingNumberOfRecords, recordsFound) {
        if (errGettingNumberOfRecords) {
            return callback(errGettingNumberOfRecords)
        }
        if (recordsFound.count == 0) {
            // use salt command to start  log Monitoring which is a nodejs process
            fileLogger.info('zero records found')
            fileLogger.info('use salt command to start  log-Monitoring which is a nodejs process')
            saltMinionCommand(IP, function (errStartingSaltProcess, processStarted) {
                if (errStartingSaltProcess) {
                    fileLogger.info("the salt Minion throws the following err")
                    fileLogger.error(errStartingSaltProcess)
                    return next(errStartingSaltProcess)
                }

                fileLogger.info("salt process started" + JSON.stringify(processStarted))

            })
        }

        determineIfkeyExistsInRedis(IP, function (errWhileCheckingIfKeyExists, keyInformation) {
            if (errWhileCheckingIfKeyExists) {
                return callback(errWhileCheckingIfKeyExists)
            }
            if (keyInformation) {
                //append
                createRedisSetForStopProcessing(IP, ID, function (err, data) {
                    if (err) {
                        fileLogger.error(err)
                        return callback(err)
                    }

                    fetchHashes(IP, function (errFetchingHash, hashData) {
                        if (errFetchingHash) {
                            return callback(errFetchingHash)
                        }
                        if (hashData == 'awaitingConnection') {
                            fileLogger.info("status is awaitingConnection in stop Process  ")

                        }
                        if (hashData == 'connected') {
                            // emit immediately
                            fileLogger.info("status is connected , hence emitting stopProcessing events")
                            //io.to(sessionid).emit("stopProcessing", ID)
                            sendAllData('log_monitoring', 'logs', function (errOnReconnectStatus, allESData) {
                                if (errOnReconnectStatus) {
                                    return callback(errOnReconnectStatus)
                                }
                                fileLogger.info('emitting the following data on reconnect:' + JSON.stringify(allESData))
                                fileLogger.info(allESData)
                                if (allESData.length > 0) {
                                    allESData.forEach(function (itemToEmit) {
                                        io.to(sessionid).emit('startProcessing', itemToEmit)
                                    })
                                }

                                io.to(sessionid).emit("stopProcessing", ID)

                            })

                        }


//                        if (hashData == 'reconnected') {
//                            fileLogger.info("status is reconnected")
//                            sendAllData('log_monitoring', 'logs', function (errOnReconnectStatus, allESData) {
//                                if (errOnReconnectStatus) {
//                                    return callback(errOnReconnectStatus)
//                                }
//                                fileLogger.info('emitting the following data on reconnect:' + JSON.stringify(allESData))
//                                socket.emit('startProcessing', allESData)
//                            })
//                        }
                    })
                })
            } else {
                //do nothing
                //and emit immediately
                fileLogger.info("no such IP exists in redis:" + IP)
                fileLogger.info("therefore creating redis set for stop processing for the IP:" + IP)
                createRedisHash(IP, function (errWhileCreatingRedisHash, redisHashCreated) {
                    if (errWhileCreatingRedisHash) {
                        logger.error(errWhileCreatingRedisHash)
                        return callback(errWhileCreatingRedisHash)
                    }
                    createRedisSetForStopProcessing(IP, ID, function (errCreatingRedisSet, setCreated) {
                        if (errCreatingRedisSet) {
                            return callback(errCreatingRedisSet)
                        }
                    })
                })
            }

        })
    })
}


function workFlowForAddPattern(sessionid, IP, ID, pattern, callback) {

    getNumberOfRecordsStored(IP, function (errGettingNumberOfRecords, recordsFound) {
        if (errGettingNumberOfRecords) {
            return callback(errGettingNumberOfRecords)
        }
        if (recordsFound.count == 0) {
            // use salt command to start  log Monitoring which is a nodejs process
            fileLogger.info('zero records found')
            fileLogger.info('use salt command to start  log-Monitoring which is a nodejs process')
            saltMinionCommand(IP, function (errStartingSaltProcess, processStarted) {
                if (errStartingSaltProcess) {
                    fileLogger.info("the salt eMinion throws the following err")
                    logger.error(errStartingSaltProcess)
                    return callback(errStartingSaltProcess)
                }

                fileLogger.info("salt process started" + JSON.stringify(processStarted))

            })
        }
        fileLogger.info("the count of records in elastic search is" + recordsFound.count)
        fileLogger.info("checking to see if " + IP + " " + "exists in redis")
        determineIfkeyExistsInRedis(IP, function (errWhileCheckingIfKeyExists, keyInformation) {
            if (errWhileCheckingIfKeyExists) {
                return callback(errWhileCheckingIfKeyExists)
            }
            if (keyInformation) {
                fileLogger.info("the IP exists in redis:" + IP)
                fileLogger.info('appending to hash')
                createRedisHashForAddPattern(IP, ID, pattern, function (errCreatingRedisSet, setCreated) {
                    if (errCreatingRedisSet) {
                        return callback(errCreatingRedisSet)
                    }

                    fetchHashes(IP, function (errFetchingHash, hashData) {
                        if (errFetchingHash) {
                            return callback(errFetchingHash)
                        }
                        fileLogger.info("the status for the given IP:" + IP + " " + "in redis is" + hashData)
                        if (hashData == 'awaitingConnection') {
                            fileLogger.info("status is awaitingConnection for add pattern event")
                        }
                        if (hashData == 'connected') {
                            // emit immediately
                            fileLogger.info("the status is connected , hence emitting add pattern events")
                            sendAllData('log_monitoring', 'logs', function (errOnReconnectStatus, allESData) {
                                if (errOnReconnectStatus) {
                                    return callback(errOnReconnectStatus)
                                }
                                fileLogger.info('emitting the following data on reconnect:' + JSON.stringify(allESData))
                                fileLogger.info(allESData)
                                if (allESData.length > 0) {
                                    allESData.forEach(function (itemToEmit) {
                                        io.to(sessionid).emit('startProcessing', itemToEmit)
                                    })
                                }


                                io.to(sessionid).emit("addPattern", {"id": ID, "pattern": pattern})
                            })

                        }
//                        if (hashData == 'reconnected') {
//                            fileLogger.info("status is reconnected")
//                            sendAllData('log_monitoring', 'logs', function (errOnReconnectStatus, allESData) {
//                                if (errOnReconnectStatus) {
//                                    return callback(errOnReconnectStatus)
//                                }
//                                fileLogger.info('emitting the following data on reconnect:' + JSON.stringify(allESData))
//                                socket.emit('startProcessing', allESData)
//                            })
//                        }
                    })
                })
            } else {
                fileLogger.info("no such IP exists in redis:" + IP)
                fileLogger.info("therefore creating redis set for add pattern for the IP:" + IP)
                createRedisHash(IP, function (errWhileCreatingRedisHash, redisHashCreated) {
                    if (errWhileCreatingRedisHash) {
                        logger.error(errWhileCreatingRedisHash)
                        return callback(errWhileCreatingRedisHash)
                    }
                    createRedisHashForAddPattern(IP, ID, pattern, function (errCreatingRedisSet, setCreated) {
                        if (errCreatingRedisSet) {
                            return callback(errCreatingRedisSet)
                        }
                    })
                })

            }

        })
    })
}


function createRedisSetForStopProcessing(IP, ID, callback) {
    fileLogger.info("create redis set for stop processing event called with params " + IP + " " + ID)
    redisSubmit.SADD(IP + "_stop_processing", ID, function (err, res) {
        if (err) {
            return callback(err)
        }
        redisSubmit.expire(IP + "_stop_processing", 300)
        return callback(null, res)
    })
}


function createRedisHashForAddPattern(IP, ID, pattern, callback) {
    fileLogger.info("create redis set for add pattern event called with params " + IP + " " + ID)
    redisSubmit.HSET(IP + "_add_pattern", ID, pattern, function (err, res) {
        if (err) {
            return callback(err)
        }
        redisSubmit.expire(IP + "_add_pattern", 300)
        return callback(null, res)
    })
}
//unused
function updateProcessingFlag(id, callback) {
    var processing = 0
    var inlineScript = "ctx._source.processing='" + processing + "'";
    elasticClient.updateByQuery({
        index: "log_monitoring",
        type: "logs",
        body: {
            script: {
                inline: inlineScript,

            },
            query: {
                match: {
                    _id: id
                }
            }
        }
    }, function (err, response) {
        if (err) {
            return callback(err)
        }
        return callback(null, response)
    })
}

//unused
function getDataBasedOnIDs(listOfIDs, callback) {
    elasticClient.search({
        index: "log_monitoring",
        type: "logs",
        body: {
            query: {
                terms: {
                    "_id": listOfIDs
                }
            }
        }
    }, function (err, response) {
        if (err) {
            logger.error(err)
            return callback(err)
        }

        var results = []
        response.hits.hits.forEach(function (item) {
            var source = item._source
            source._id = item._id
            results.push(source)
        })
        return callback(null, results)
    })
}

function isFilePathUnique(product, vendor, location, IP, path, index, type, callback) {
    logger.info("is filePath unique called with: " + product + " " + vendor + " " + location + " " + IP + " " + path + " " + index + " " + type)
    elasticClient.count({
        index: index, //"log_monitoring",
        type: type, //logs",
        body: {
            query: {
                bool: {
                    must: [
                        {term: {"PRODUCT_NAME": product}},
                        {term: {"VENDOR_NAME": vendor}},
                        {term: {"LOCATION": location}},
                        {term: {"IP_ADDRESS": IP}},
                        {term: {"path": path}}


                    ]

                }
            }
        }
    }, function (err, response) {
        if (err) {
            return callback(err)
        }
        if (response.count != 0) {
            return callback(null, {"status": 0, "data": "file path  is not unique"})
        } else {
            return callback(null, {"status": 1, "data": "success"})
        }
    })
}


function isURLUnique(product, vendor, location, IP, URL, index, type, callback) {
    logger.info("isURL unique called with:" + product + " " + vendor + " " + location + " " + IP + " " + URL + " " + index + " " + type)
    elasticClient.count({
        index: index, //"log_monitoring",
        type: type, //logs",
        body: {
            query: {
                bool: {
                    must: [
                        {term: {"PRODUCT_NAME": product}},
                        {term: {"VENDOR_NAME": vendor}},
                        {term: {"LOCATION": location}},
                        {term: {"IP_ADDRESS": IP}},
                        {match: {"URL": URL}}


                    ]

                }
            }
        }
    }, function (err, response) {
        if (err) {
            return callback(err)
        }
        if (response.count != 0) {
            return callback(null, {"status": 0, "msg": "URL  is not unique"})
        } else {
            return callback(null, {"status": 1})
        }
    })
}


function isDisplayNameUnique(displayName, index, type, callback) {
    elasticClient.count({
        index: index,
        type: type,
        body: {
            query: {
                match: {"displayName": displayName}

            }
        }
    }, function (err, response) {
        if (err) {
            return callback(err)
        }
        if (response.count != 0) {
            callback(null, {"status": 0, "msg": "displayName is not  unique"})
        } else {
            callback(null, {"status": 1})

        }
    })
}


function isPatternUniqueForCounters(pattern, product, vendor, location, IP, index, type, callback) {
    elasticClient.count({
        index: index, //"log_monitoring",
        type: type, //"logs",
        body: {
            query: {
                bool: {
                    must: [
                        {term: {"PRODUCT_NAME": product}},
                        {term: {"VENDOR_NAME": vendor}},
                        {term: {"LOCATION": location}},
                        {term: {"IP_ADDRESS": IP}},
                        {term: {"pattern": pattern}}


                    ]

                }
            }
        }
    }, function (err, response) {
        if (err) {
            return callback(err)
        }
        if (response.count != 0) {
            return callback(null, {"status": 0, "msg": "pattern  is not unique"})
        } else {
            return callback(null, {"status": 1})
        }
    })
}


function isPatternUnique(pattern, product, vendor, location, IP, index, type, callback) {
    elasticClient.count({
        index: index, //"log_monitoring",
        type: type, //"logs",
        body: {
            query: {
                bool: {
                    must: [
                        {term: {"PRODUCT_NAME": product}},
                        {term: {"VENDOR_NAME": vendor}},
                        {term: {"LOCATION": location}},
                        {term: {"IP_ADDRESS": IP}},
                        {match_phrase: {"pattern": pattern[0]}}


                    ]

                }
            }
        }
    }, function (err, response) {
        if (err) {
            return callback(err)
        }
        if (response.count != 0) {
            return callback(null, {"status": 0, "msg": "pattern  is not unique"})
        } else {
            return callback(null, {"status": 1})
        }
    })
}

function validateWhenAddingLogMonitoringData(product, location, vendor, IP, pattern, path, displayName, index, type, callback) {
    fileLogger.info("validateWhen adding log monitoring is called with the following data:")
    fileLogger.info(product + " " + location + " " + vendor + " " + IP + " " + pattern + " " + path + " " + displayName)
    isDisplayNameUnique(displayName, index, type, function (errFindingDisplayName, displayNameFound) {
        if (errFindingDisplayName) {
            fileLogger.error(errFindingDisplayName)
            return callback(new errorHandler.serverError())
        }
        if (displayNameFound.status == 1) {
            isFilePathUnique(product, vendor, location, IP, path, index, type, function (errFindingFilePath, filePathFound) {
                if (errFindingFilePath) {
                    fileLogger.error(errFindingFilePath)
                    return callback(new errorHandler.serverError())
                }
                if (filePathFound.status == 1) {
                    isPatternUnique(pattern, product, vendor, location, IP, index, type, function (errFindingPattern, patternFound) {
                        if (errFindingPattern) {
                            fileLogger.error(errFindingPattern)
                            return callback(new errorHandler.serverError())
                        }
                        if (patternFound.status == 1) {
                            return callback(null, patternFound) //status:1
                        }
                        else {
                            return callback(new errorHandler.patternIsNotUnique())
                        }

                    })
                } else {
                    return callback(new errorHandler.filePath())
                }
            })
        } else {
            return callback(new errorHandler.displayName())
        }
    })
}


function saltMinionCommand(IP, callback) {

    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0"


    var url = "https://10.225.253.133:9999/run";
    var headers = {
        'Accept': 'application/x-yaml',
        'X-Auth-Token': "41b9539436faae8016c305c2f875b31e47a23d93",
        'Content-type': "application/json"
    };

    fileLogger.info("running the salt Minion command function with the url:" + url)
    fileLogger.info("running the salt Minion command function with the headers:" + headers)

    request({
        url: url,
        headers: headers,
        method: "POST",
        json: true,
        body: [{
            "client": "local",
            "tgt": IP,
            "fun": "cmd.run",
            //cd /root/.GEMS/ && pm2 start audit.sh --name="GEMS-audit"
            //   "kwarg": {"cmd": 'cd /opt/FileProcessor && node index.js', "stdin": "y"},
            "kwarg": {"cmd": 'cd /opt/FileProcessor && sh Node.sh', "stdin": "y"},
            "username": "salt",
            "password": "salt",
            "eauth": "pam"
        }]
    }, function (err, httpResponse, body) {
        if (err) {
            return callback(err)
            //res.send("Failed");
        }
        return callback(null, body)

    })
}


function deleteDataFromMonitoring(_id, index, type, callback) {
    elasticClient.delete({
        index: index,
        type: type,
        id: _id
    }, function (err, data) {
        if (err) {
            return callback(err)
        }
        return callback(null, {"status": 1})
        //res.json({"status": 1})


    })
}


function addMonitoringDataWithTypeHTTP(req, res, next) {
    HTTPLogger.info("addmonitoringDataWithHTTP called with the data:" + JSON.stringify(req.body))
    var method = req.body.method
    var URL = req.body.url

    req.body.TIME = moment().format('YYYY-MM-DD HH:mm:ss')
    var frequency = req.body.frequency
    var frequencyString = frequency.duration + " " + frequency.format
    HTTPLogger.info("the frequency string is :" + frequencyString)

    validationForMonitoringDataHTTP(req.body.product, req.body.vendor, req.body.location, req.body.IP, req.body.pattern, req.body.url, req.body.displayName, function (validationErr, validated) {
        if (validationErr) {

            HTTPLogger.error(validationErr)
            return next(validationErr)
        }
        else {
            if (req.body.parameter.length == 0) {
                var EsObj = {
                    "PRODUCT_NAME": req.body.product,
                    "VENDOR_NAME": req.body.vendor,
                    "LOCATION": req.body.location,
                    "IP_ADDRESS": req.body.IP,
                    "severity": req.body.severity,
                    "displayName": req.body.displayName,
                    "URL": req.body.url,
                    "method": req.body.method,
                    "format": req.body.format,
                    "frequency": req.body.frequency,

                    // "parameter": req.body.parameter,
                    "pattern": req.body.pattern,
                    "caseSensitive": req.body.caseSensitive,
                    "TYPE": req.body.TYPE,
                    "patternArray": req.body.patternArray,
                    "patternPreview": req.body.patternPreview,
                    "parameterStatus": req.body.parameterStatus


                }
            } else if (typeof req.body.parameter[0] == 'object')
            {
                console.log("application/json")

                var EsObj = {
                    "PRODUCT_NAME": req.body.product,
                    "VENDOR_NAME": req.body.vendor,
                    "LOCATION": req.body.location,
                    "IP_ADDRESS": req.body.IP,
                    "severity": req.body.severity,
                    "displayName": req.body.displayName,
                    "URL": req.body.url,
                    "method": req.body.method,
                    "format": req.body.format,
                    "frequency": req.body.frequency,

                    "parameter": req.body.parameter,
                    "pattern": req.body.pattern,
                    "caseSensitive": req.body.caseSensitive,
                    "TYPE": req.body.TYPE,
                    "patternArray": req.body.patternArray,
                        "patternPreview": req.body.patternPreview,
                    "parameterStatus": req.body.parameterStatus

                }
            } else {
                var EsObj = {
                    "PRODUCT_NAME": req.body.product,
                    "VENDOR_NAME": req.body.vendor,
                    "LOCATION": req.body.location,
                    "IP_ADDRESS": req.body.IP,
                    "severity": req.body.severity,
                    "displayName": req.body.displayName,
                    "URL": req.body.url,
                    "method": req.body.method,
                    "format": req.body.format,
                    "frequency": req.body.frequency,

                    "textParameter": req.body.parameter,
                    "pattern": req.body.pattern,
                    "caseSensitive": req.body.caseSensitive,
                    "TYPE": req.body.TYPE,
                    "patternArray": req.body.patternArray,
                        "patternPreview": req.body.patternPreview,
                        "parameterStatus": req.body.parameterStatus

                }
            }
            HTTPLogger.info("The ES object inserted to log_monitoring_http is")
            HTTPLogger.info(EsObj)


            insertMonitoringData("log_monitoring_http", "logs", EsObj, function (err, data) {
                if (err) {
                    HTTPLogger.error(err)
                    return next(err)
                    //new errorHandler.serverError())
                }
                var job_id = data._id
                HTTPLogger.info("_id of the inserted record:" + job_id)

                var agendaObject = {
                    "job_id": job_id,
                    'http_method': method,
                    "http_url": URL,
                    "method": req.body.method,
                    "contentType": req.body.format,
                    "params": req.body.parameter,
                    "caseSensitive": req.body.caseSensitive,
                    "pattern": req.body.pattern
                    // "params":req.body.parameters
                }
                if (req.body.parameter != undefined || req.body.parameter != null) {
                    agendaObject.params = req.body.parameter
                }

                //  logger.info("the agenda object is:" + JSON.stringify(agendaObject))
                agenda.every(frequencyString, 'schedule_http_request', agendaObject)
//        agenda.every('1 minute', 'schedule_http_request', agendaObject,function(err,data){
//            if(err){
//                logger.info(err)
//            }
//            console.log(data)
//        })
                res.json({"status": 1})
            })
        }
    })
}


function validationForMonitoringDataHTTP(product, vendor, location, IP, pattern, URL, displayName, callback) {
    HTTPLogger.info("validateWhen adding log monitoring data with HTTP is called with the following data:")
    HTTPLogger.info("product:" + product + " " + "vendor:" + vendor + " " + "location:" + location + " " + "IP:" + IP + "" + "pattern:" + pattern + " " + "URL:" + URL + " " + "displayName:" + displayName)
    var product = product
    var location = location
    var vendor = vendor
    var IP = IP
    var pattern = pattern
    var URL = URL

    isDisplayNameUnique(displayName, 'log_monitoring_http', 'logs', function (errFindingDisplayName, displayNameFound) {
        if (errFindingDisplayName) {
            HTTPLogger.error(errFindingDisplayName)
            return callback(new errorHandler.serverError())
        }
        if (displayNameFound.status == 1) {
            isURLUnique(product, vendor, location, IP, URL, 'log_monitoring_http', 'logs', function (errFindingURL, URLFound) {
                if (errFindingURL) {
                    HTTPLogger.error(errFindingURL)
                    return callback(new errorHandler.serverError())
                }
                if (URLFound.status == 1) {
                    //check for pattern

                    isPatternUnique(pattern, product, vendor, location, IP, 'log_monitoring_http', 'logs', function (errFindingPattern, patternFound) {
                        if (errFindingPattern) {
                            HTTPLogger.error("pattern unique function fails" + JSON.stringify(errFindingPattern))
                            return callback(new errorHandler.serverError())
                        }

                        if (patternFound.status == 1) {
                            return callback(null, patternFound)
                        }
                        else {
                            return callback(new errorHandler.patternIsNotUnique())
                        }
                    })
                } else {
                    return callback(new errorHandler.URLIsNotUnique())
                }
            })
        } else {
            return callback(new errorHandler.displayName())
        }
    })
}


function formulateObj(esObj, callback) {

    counterLogger.info('formulate object called:' + JSON.stringify(esObj))


    var headers = []
    var patternArray = esObj.patternArray


    patternArray.forEach(function (eachObj) {
        var name = eachObj.fieldName
        var field = eachObj.fieldNo
        var headersObj = {name: name, field: field}
        headers.push(headersObj)

    })

    var ServerInfo = {
        "Product": esObj.PRODUCT_NAME,
        "Location": esObj.LOCATION,
        "Vendor": esObj.VENDOR_NAME,
        "KafkaIP": "172.16.23.27:9092",
        "IPAddress": esObj.IP_ADDRESS
    }
    var RuleName = {
        "name": esObj.displayName //displayName
    }
    var requestType = {
        "requestType": esObj.requestType
    }

    var Total_fields = {
        "Number_of_fields": esObj.totalNoOfFields
    }
    var Field_Seperator = {
        "Field_Seperator": esObj.delimiter //delimiter
    }

    var Input_File = {
        "Name": esObj.fileName, //fileName
        "Format": esObj.fileFormat,
        "Path": esObj.filePath
    }

    var finalObj = {
        "headers": headers, "RuleName": RuleName, "Total_fields": Total_fields, "Field_Seperator": Field_Seperator
        , "ServerInfo": ServerInfo, "Input_File": Input_File, "requestType": requestType
    }
    counterLogger.info("the final Obj inside formulate Obj function is:" + JSON.stringify(finalObj))

    return callback(null, finalObj)


}

function addMonitoringDataForCounters(req, res, next) {
    counterLogger.info("addMonitoringCounters called"+util.format(req.body))
    validateCounters(req.body.product, req.body.location, req.body.vendor, req.body.IP, req.body.pattern, req.body.path, req.body.displayName, 'log_monitoring_counters', 'logs', function (validationErr, validated) {
        if (validationErr) {
            return next(validationErr)
        }


        var esObj =
            {
                "PRODUCT_NAME": req.body.product,
                "LOCATION": req.body.location,
                "VENDOR_NAME": req.body.vendor,
                "IP_ADDRESS": req.body.IP,
                "fileName": req.body.fileName,
                "fileFormat": req.body.fileFormat,
                "delimiter": req.body.delimiter,
                "pattern": req.body.pattern,
                // "method": req.body.method,
                "path": req.body.path,
                "displayName": req.body.displayName,
                "TYPE": req.body.TYPE,
                "patternArray": req.body.patternArray,
                "patternPreview": req.body.patternPreview,
                "totalNoOfFields": req.body.totalNoOfFields,
                "TIME": moment().format('YYYY-MM-DD HH:mm:ss'),
                "requestType": "new",
                "isDateChecked": req.body.isDateChecked,
                "dateFieldNo": req.body.dateFieldNo,
                "dateFormat": req.body.dateFormat


            }
        var redisObj = {
            "fileName": req.body.fileName,
            "path": req.body.path,
            "patternArray": JSON.stringify(req.body.patternArray),
            "TYPE": req.body.TYPE,
            "displayName": req.body.displayName,
            "PRODUCT_NAME": req.body.product,
            "LOCATION": req.body.location,
            "VENDOR_NAME": req.body.vendor,
            "IP_ADDRESS": req.body.IP,
            "delimiter": req.body.delimiter,
            "fileFormat": req.body.fileFormat,
            "pattern": req.body.pattern,
            "totalNoOfFields": req.body.totalNoOfFields
        }


        var IP = req.body.IP

// insert required data into redis and fetch from redis in kafka.js
        insertMonitoringData('log_monitoring_counters', 'logs', esObj, function (err, data) {
            if (err) {
                counterLogger.error(err.stack)
                return next(err)
            }
            res.json({"status": 1})
            //formulate RedisObj with -id of the inserted record
            redisObj._id = data._id
            counterLogger.info("the redis object is:" + JSON.stringify(redisObj))
            insertCounterDataToRedis(redisObj, function (errWhileInsertingRedisHash, redisHashInserted) {
                if (err) {
                    return next(errWhileInsertingRedisHash)
                }

                formulateObj(esObj, function (errFormulatingObj, objFormulated) {

                    callScriptWhenAddingCounterData(IP, objFormulated, function (errCallingRequest, allAPIRequestsSuccessful) {
                        if (errCallingRequest) {
                            counterLogger.error(errCallingRequest)
                            return next(errCallingRequest)
                        }
                        counterLogger.info("allAPIRequests are Successful")

                    })
                })
            })


        })
    })

}


function insertCounterDataToRedis(redisObj, callback) {
    var redisHashKey = redisObj.PRODUCT_NAME + '_' + redisObj.LOCATION + '_' + redisObj.VENDOR_NAME + '_' + redisObj.IP_ADDRESS + '_' + redisObj.displayName
    redisSubmit.hmset(redisHashKey, "TYPE", redisObj.TYPE, "filePath", redisObj.filePath, "fileName", redisObj.fileName, "patternArray", redisObj.patternArray, "_id", redisObj._id, "delimiter", redisObj.delimiter, function (err, res) {
        if (err) {
            return callback(err)
        }
        return callback(null, res)
    })
}
function deleteDataForCounterMonitoring(req, res, next) {
    var _id = req.body._id
    var IP = req.body.IP
    var displayName = req.body.displayName

    /* since the redis modification PLV IP and displayName are expected*/
    var product = req.body.product
    var location = req.body.location
    var vendor = req.body.vendor
    var IP = req.body.IP
    var displayName = req.body.displayName
    var redisKey = product + '_' + location + '_' + vendor + '_' + IP + '_' + displayName


    deleteDataFromMonitoring(_id, 'log_monitoring_counters', 'logs', function (err, data) {
        if (err) {
            counterLogger.error(err.stack)
            return next(err)
        }
        res.json({"status": 1})
        var RuleName = {"name": displayName}
        var requestType = {"requestType": "delete"}
        var obj = {"requestType": requestType, "RuleName": RuleName}

        // delete redis hash code
        var redisHashKey = req.body.product + '_' + req.body.location + '_' + req.body.vendor + '_' + req.body.IP + '_' + req.body.displayName
        counterLogger.info("the redis key is:" + redisHashKey)
        deleteRedisHash(redisHashKey, function (errWhileDeletingHash, hashDeleted) {
            if (errWhileDeletingHash) {
                return next(errWhileDeletingHash)
            }
            callScriptWhenAddingCounterData(IP, obj, function (errCallingRequest, allAPIRequestsSuccessful) {
                if (errCallingRequest) {
                    counterLogger.error(errCallingRequest)
                    return next(errCallingRequest)
                }
                counterLogger.info("allAPIRequests are Successful")
            })

        })

    })
}


function deleteRedisHash(key, callback) {
    redisSubmit.del(key, function (err, res) {
        if (err) {
            return callback(err)
        }
        return callback(null, res)
    })
}


function editCounterMonitoringData(req, res, next) {
    counterLogger.info("EDIT COUNTER MONITORING called:" + JSON.stringify(req.body))
    //expects _id and pattern field
    //deletes the field and inserts it again
    var pattern = req.body.pattern
    var patternArray = req.body.patternArray
    var patternPreview = req.body.patternPreview
    var _id = req.body._id

    /*
     * code changes for using redis expects more parameters i.e PLV ,IP and displayName
     */
    var product = req.body.product
    var location = req.body.location
    var vendor = req.body.vendor
    var IP = req.body.IP
    var displayName = req.body.displayName
    var redisKey = product + '_' + location + '_' + vendor + '_' + IP + '_' + displayName


    var theScript = {
        "inline": "ctx._source.remove(\"pattern\");" + "ctx._source.remove(\"patternPreview\");" + "ctx._source.remove(\"patternArray\");"

    }

    elasticClient.updateByQuery({
        index: 'log_monitoring_counters',
        type: 'logs',
        body: {
            script: theScript,
            query: {
                term: {
                    _id: _id

                }
            }
        }
    }, function (error, response) {
        if (error) {
            counterLogger.error(error.stack)
            return next(error)
        }

        elasticClient.update({
            index: 'log_monitoring_counters',
            type: 'logs',
            id: _id,
            body: {
                doc: {
                    pattern: pattern,
                    patternArray: patternArray,
                    patternPreview: patternPreview
                }


            }
        }, function (errPartialUpdate, partialUpdateSuccessful) {
            if (errPartialUpdate) {
                counterLogger.error(errPartialUpdate)
                return next(errPartialUpdate)
            }
            res.json({"status": 1})
            //add the code here
            counterLogger.info("THE ID is:" + _id)

            editHash(redisKey, 'patternArray', patternArray, pattern, pattern, function (errUpdatingHash, updateSuccessful) {
                if (errUpdatingHash) {
                    return next(errUpdatingHash)
                }
                fetchHashData(redisKey, function (errFetchingUpdatedData, updatedData) {
                    if (errFetchingUpdatedData) {
                        return next(errFetchingUpdatedData)
                    }
                    var esObj =
                        {
                            "PRODUCT_NAME": product,
                            "LOCATION": location,
                            "VENDOR_NAME": vendor,
                            "IP_ADDRESS": IP,
                            "fileName": updatedData.fileName,
                            "fileFormat": updatedData.fileFormat,
                            "delimiter": updatedData.delimiter,
                            "pattern": pattern,
                            // "method": req.body.method,
                            "filePath": updatedData.filePath,
                            "displayName": displayName,
                            "TYPE": updatedData.TYPE,
                            "patternArray": patternArray,
                            "totalNoOfFields": updatedData.totalNoOfFields,
                            "TIME": moment().format('YYYY-MM-DD HH:mm:ss'),
                            "requestType": "update"


                        }
                    formulateObj(esObj, function (errFormulatingObj, objFormulated) {

                        counterLogger.info("the formulated object in editCounters is:" + objFormulated)
                        counterLogger.info("calling script in editCounters with the following data:" + IP, " " + JSON.stringify(objFormulated))
                        callScriptWhenAddingCounterData(IP, objFormulated, function (errCallingRequest, allAPIRequestsSuccessful) {
                            if (errCallingRequest) {
                                counterLogger.error(errCallingRequest)
                                return next(errCallingRequest)
                            }
                            counterLogger.info("requests are successful:" + JSON.stringify(allAPIRequestsSuccessful))

                        })
                    })
                })

            })
            //the below code is no longer needed since data is fetched from redis
            // fetchDataBasedOnID('log_monitoring_counters', 'logs', _id, function (errFetchingUpdatedData, updatedData) {
            //     if (errFetchingUpdatedData) {
            //         logger.error(errFetchingUpdatedData)
            //         return next(errFetchingUpdatedData)
            //     }
            //     // res.json({"status": 1})
            //     var IP = updatedData.hits.hits[0]._source.IP_ADDRESS
            //     counterLogger.info("the params passed to formauleObj in edit Counters is:" + JSON.stringify(esObj))
            //     var esObj =
            //             {
            //                 "PRODUCT_NAME": updatedData.hits.hits[0]._source.PRODUCT_NAME,
            //                 "LOCATION": updatedData.hits.hits[0]._source.LOCATION,
            //                 "VENDOR_NAME": updatedData.hits.hits[0]._source.VENDOR_NAME,
            //                 "IP_ADDRESS": updatedData.hits.hits[0]._source.IP_ADDRESS,
            //                 "fileName": updatedData.hits.hits[0]._source.fileName,
            //                 "fileFormat": updatedData.hits.hits[0]._source.fileFormat,
            //                 "delimiter": updatedData.hits.hits[0]._source.delimiter,
            //                 "pattern": pattern,
            //                 // "method": req.body.method,
            //                 "filePath": updatedData.hits.hits[0]._source.filePath,
            //                 "displayName": updatedData.hits.hits[0]._source.displayName,
            //                 "TYPE": updatedData.hits.hits[0]._source.TYPE,
            //                 "patternArray": patternArray,
            //                 "totalNoOfFields": updatedData.hits.hits[0]._source.totalNoOfFields,
            //                 "TIME": moment().format('YYYY-MM-DD HH:mm:ss'),
            //                 "requestType": "update"
            //
            //
            //             }
            //     counterLogger.info("fetchDataBasedonID returns" + JSON.stringify(esObj))
            //     //modify redis data
            //     formulateObj(esObj, function (errFormulatingObj, objFormulated) {
            //
            //         counterLogger.info("the formulated object in editCounters is:" + objFormulated)
            //         counterLogger.info("calling script in editCounters with the following data:" + IP, " " + JSON.stringify(objFormulated))
            //         callScriptWhenAddingCounterData(IP, objFormulated, function (errCallingRequest, allAPIRequestsSuccessful) {
            //             if (errCallingRequest) {
            //                 counterLogger.error(errCallingRequest)
            //                 return next(errCallingRequest)
            //             }
            //             counterLogger.info("requests are successful:" + JSON.stringify(allAPIRequestsSuccessful))
            //
            //         })
            //     })
            // })


        })


    })


}





function deleteDataForHTTPMonitoring(req, res, next) {
    var _id = req.body._id
    HTTPLogger.info("delete data for HTTP Monitoring called with:" + _id)

    deleteDataFromMonitoring(_id, 'log_monitoring_http', 'logs', function (err, data) {
        if (err) {
            HTTPLogger.error(err)
            return next(err)

        }

        agenda.cancel({job_id: _id}, function (error, numRemoved) {
            if (error) {

                HTTPLogger.error(error.stack)
                return next(error)
            }
            HTTPLogger.info("agenda has stopped the processing of these many number of jobs:" + numRemoved)
            res.json({"status": 1})
        });
    })

}


function updatePatternForHTTPMonitoring(req, res, next) {
    var _id = req.body._id
    var pattern = req.body.pattern
    var patternArray = req.body.patternArray
    var patternPreview = req.body.patternPreview
    HTTPLogger.info("updatePatternForHTTPMonitoring")
    HTTPLogger.info(_id + " " + pattern + " " + patternArray + " " + patternPreview)
//"inline": "ctx._source.condition = " + "\"" + conditionInEs + "\"" + ";" + "ctx._source.remove(\"criteria\");"
    var theScript = {
        "inline": "ctx._source.remove(\"pattern\");" + "ctx._source.remove(\"patternPreview\");" + "ctx._source.remove(\"patternArray\");"

    }
    elasticClient.updateByQuery({
        index: 'log_monitoring_http',
        type: 'logs',
        body: {
            script: theScript,
            query: {
                term: {
                    _id: _id

                }
            }
        }
    }, function (error, response) {
        if (error) {
            HTTPLogger.error(error)

            return next(error)
        }

        elasticClient.update({
            index: 'log_monitoring_http',
            type: 'logs',
            id: _id,
            body: {
                doc: {
                    pattern: req.body.pattern,
                    patternArray: req.body.patternArray,
                    patternPreview: req.body.patternPreview
                }
            }
        }, function (errPartialUpdate, partialUpdateSuccessful) {
            if (errPartialUpdate) {
                HTTPLogger.error(errPartialUpdate)
                return next(errPartialUpdate)
                //return HTTPLogger.error(errPartialUpdate.stack)
            }
            // mongodb
            MongoClient.connect(url, function (err, db) {
                if (err) {
                    HTTPLogger.error(err)
                    return next(err)
                }

                db.collection("schedules").update({job_id: _id}, {$set: {'data.pattern': pattern}}, function (errUpdatingMongo, result) {
                    if (errUpdatingMongo) {
                        HTTPLogger.error(errUpdatingMongo)
                        return next(errUpdatingMongo)
                    }

                    db.close();
                    res.json({"status": 1})
                });
            });

        })
    })
}


function callScriptWhenAddingCounterData(ip, json, callback) {

    counterLogger.info("inside callscript function")
    counterLogger.silly("Writing file Obj :" + JSON.stringify(json));
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
    fs.writeFile('/root/' + ip + '.json', JSON.stringify(json), 'utf8', function (err) {
        if (err) {
            //logger.silly(err);

            return callback(err.stack)
        } else {
            counterLogger.silly("File saved for IP " + ip + " successfully");
            client.scp('/root/' + ip + '.json', {
                host: '10.225.253.133',
                username: 'root',
                password: 'teledna',
                path: '/srv/salt'
            }, function (err) {
                if (err) {
                    return callback(err.stack);
                } else {
                    request({
                        url: config.counterMonitoring.api, //"https://10.225.253.133:9999/run",
                        headers: {
                            'Accept': config.counterMonitoring.accept, //'application/x-yaml',
                            'X-Auth-Token': config.counterMonitoring.authToken,
                            'Content-type': config.counterMonitoring.contentType,
                        },
                        method: config.counterMonitoring.method,
                        json: true,
                        body: [{
                            "client": "local",
                            "tgt": ip,
                            "fun": "cp.get_file",
                            "kwarg": {"path": "salt://" + ip + ".json", "dest": "/root/" + ip + ".json"},
                            "username": "salt",
                            "password": "salt",
                            "eauth": "pam"
                        }]
                    }, function (err, httpResponse, body) {
                        if (err) {
                            return callback(err.stack);
                        } else {
                            var args = '/root/' + ip + '.json /etc/rsyslog.conf';
                            request({
                                url: config.counterMonitoring.api, //"https://10.225.253.133:9999/run",
                                headers: {
                                    'Accept': config.counterMonitoring.accept,
                                    'X-Auth-Token': config.counterMonitoring.authToken, //"41b9539436faae8016c305c2f875b31e47a23d93",
                                    'Content-type': config.counterMonitoring.contentType,
                                },

                                method: config.counterMonitoring.method,
                                json: true,
                                body: [{
                                    "client": "local",
                                    "tgt": ip,
                                    "fun": "cmd.script",
                                    "kwarg": {"source": "salt://rsyslog_counter_update.py", "args": args},
                                    "username": "salt",
                                    "password": "salt",
                                    "eauth": "pam"
                                }]
                            }, function (err, httpResponse, body) {
                                if (err) {
                                    return callback(err.stack);
                                } else {
                                    counterLogger.silly("Script executed successfully");
                                    request({
                                        url: config.counterMonitoring.api, //"https://10.225.253.133:9999/run",
                                        headers: {
                                            'Accept': config.counterMonitoring.accept,
                                            'X-Auth-Token': config.counterMonitoring.authToken, //"41b9539436faae8016c305c2f875b31e47a23d93",
                                            'Content-type': config.counterMonitoring.contentType//'application/json',
                                        },
                                        method: config.counterMonitoring.method,
                                        json: true,
                                        body: [{
                                            "client": "local",
                                            "tgt": ip,
                                            "fun": "cmd.run",
                                            "kwarg": {"cmd": "service rsyslog restart", "stdin": "y"},
                                            "username": "salt",
                                            "password": "salt",
                                            "eauth": "pam"
                                        }]
                                    }, function (err, httpResponse, body) {
                                        if (err) {
                                            return callback(err.stack);
                                        } else {
                                            counterLogger.silly("Rsyslog restarted Successfully");
                                            return callback(null, 1)
                                        }
                                    })
                                }
                            })
                        }
                    });
                }
            })
        }
    });

}

function fetchAllDataES(index, type, callback) {
    logger.info("fetching all data in the ES index:" + index + " " + "with type:" + type)
    elasticClient.search({
        index: index,
        type: type,
        body: {
            query: {
                match_all: {}
            }
        }

    }, function (err, response) {
        if (err) {

            return callback(err)
        }
        logger.info("fetchALLDataEs returns:" + JSON.stringify(response))

        return callback(null, response)

    })
}


function sendAllData(index, type, callback) {
    fileLogger.info("in the sendALLdata function with index:" + index + " " + "type:" + type)
    var arr = []
    fetchAllDataES(index, type, function (err, data) {
        if (err) {
            return callback(err)
        }
        data.hits.hits.forEach(function (item) {
            if (item._source.watchDirectory == true) {

                var jsonObj = {
                    //  "fileName": fileName,
                    "path": item._source.path,
                    "IP_ADDRESS": item._source.IP_ADDRESS,
                    "pattern": item._source.pattern,
                    "caseSensitive": item._source.caseSensitive,
                    "TYPE": item._source.type,
                    "watchDirectory": item._source.watchDirectory,
                    "riseAlarm": item._source.riseAlarm,
                    //"extension":  item._source.filename.split('.')[1],
                    "processing": 1,
                    "PRODUCT_NAME": item._source.PRODUCT_NAME,
                    "LOCATION": item._source.LOCATION,
                    "VENDOR_NAME": item._source.VENDOR_NAME,
                    "id": item._id

                }
                var extension = item._source.filename.split('.')[1]
                if (extension != undefined) {
                    jsonObj.extension = extension
                }
            } else {

                var jsonObj = {

                    "path": item._source.path,
                    "fileName": item._source.fileName,
                    "IP_ADDRESS": item._source.IP_ADDRESS,
                    "pattern": item._source.pattern,
                    "caseSensitive": item._source.caseSensitive,
                    "TYPE": item._source.type,
                    "watchDirectory": item._source.watchDirectory,
                    "riseAlarm": item._source.riseAlarm,
                    //"extension":  item._source.filename.split('.')[1],
                    "processing": 1,
                    "PRODUCT_NAME": item._source.PRODUCT_NAME,
                    "LOCATION": item._source.LOCATION,
                    "VENDOR_NAME": item._source.VENDOR_NAME,
                    "id": item._id

                }
            }

            arr.push(jsonObj)

        })
        fileLogger.info("sendALLdata returns the following:" + JSON.stringify(arr))
        return callback(null, arr)

    })
}


function editHash(key, subKey1, modifiedData1, subKey2, modifiedData2, callback) {
    redisSubmit.hmset(key, subKey1, modifiedData1, subKey2, modifiedData2, function (err, res) {
        if (err) {
            return callback(err)
        }
        return callback(null, res)
    })
}


function fetchHashData(key, callback) {
    redisSubmit.hgetall(key, function (err, res) {
        if (err) {
            return callback(err)
        }
        return callback(null, res)

    })
}



function validateCounters(product, location, vendor, IP, pattern, path, displayName, index, type, callback) {
    fileLogger.info("validateWhen adding log monitoring is called with the following data:")
    fileLogger.info(product + " " + location + " " + vendor + " " + IP + " " + pattern + " " + path + " " + displayName)
    isDisplayNameUnique(displayName, index, type, function (errFindingDisplayName, displayNameFound) {
        if (errFindingDisplayName) {
            fileLogger.error(errFindingDisplayName)
            return callback(new errorHandler.serverError())
        }
        if (displayNameFound.status == 1) {
            isFilePathUnique(product, vendor, location, IP, path, index, type, function (errFindingFilePath, filePathFound) {
                if (errFindingFilePath) {
                    fileLogger.error(errFindingFilePath)
                    return callback(new errorHandler.serverError())
                }
                if (filePathFound.status == 1) {
                    isPatternUniqueForCounters(pattern, product, vendor, location, IP, index, type, function (errFindingPattern, patternFound) {
                        if (errFindingPattern) {
                            fileLogger.error(errFindingPattern)
                            return callback(new errorHandler.serverError())
                        }
                        if (patternFound.status == 1) {
                            return callback(null, patternFound) //status:1
                        }
                        else {
                            return callback(new errorHandler.patternIsNotUnique())
                        }

                    })
                } else {
                    return callback(new errorHandler.filePath())
                }
            })
        } else {
            return callback(new errorHandler.displayName())
        }
    })
}




exports.addLogMonitoringData = addLogMonitoringData
exports.editPattern = editPattern
exports.stopWatchingFile = stopWatchingFile
exports.listData = listData
exports.getMonitoringAlarms = getMonitoringAlarms

exports.deleteDataFromMonitoring = deleteDataFromMonitoring
exports.addMonitoringDataWithTypeHTTP = addMonitoringDataWithTypeHTTP
//exports.validationForMonitoringDataHTTP = validationForMonitoringDataHTTP
exports.addMonitoringDataForCounters = addMonitoringDataForCounters
exports.deleteDataForCounterMonitoring = deleteDataForCounterMonitoring
exports.editCounterMonitoringData = editCounterMonitoringData

exports.deleteDataForHTTPMonitoring = deleteDataForHTTPMonitoring
exports.updatePatternForHTTPMonitoring = updatePatternForHTTPMonitoring

