/**
 *  * Created by nareshs on 21-09-2017.
 *   */
var moment = require('moment')
var redis = require('redis')
var config = require('config')
var redisSubmit = redis.createClient(config.redis.port, config.redis.host);

var logger = require('../libs/logger').HTTPLogger
class Pattern {
    constructor(data) {
        this.regExpCond = data.caseSensitive ? 'gm' : 'gim';
        this.pattern = data.pattern;
        this.regExpArr = [];
        this.regExpArrCount = this.pattern.length;
        this.regExpFromArr();
    }
    regExpFromArr() {
        try {
            if (this.regExpArrCount > 0) {
                for (let i = 0; i < this.regExpArrCount; i++) {
                    this.regExpArr[i] = new RegExp(this.splitAndCreateRegExp(i), this.regExpCond);
                    logger.debug(`At C:Pattern, M:regExpFromArr - regexp  index ${i}, string : ${this.regExpArr[i].source}`);
                }
            }
        } catch (err) {
            logger.error(err.stack);
        }
    }
    addPattern(newPattern) {  // required when pattern is edited
        try {
            if (newPattern.length > 0) {
                this.pattern = newPattern;
                this.regExpArrCount = this.pattern.length;
                this.regExpFromArr();
            }
        } catch (err) {
            logger.error(err.stack);
        }
    }
    uniqueArr(arr) { // arr = to remove duplicate entries from log files. Helper function used in searchPattern
        var returnArr = [];
        try {
            var arrCount = arr ? arr.length : 0;
            if (arrCount > 0) {
                for (let i = 0; i < arrCount; i++) {
                    if (returnArr.indexOf(arr[i]) < 0) {
                        returnArr.push(arr[i]);
                    }
                }
            }
            return returnArr;
        } catch (err) {
            logger.error(err.stack);
            return returnArr;
        }
    }

    //
    searchPattern(data, _id) {  // alertSystem ? data represents the api output from sendHTTP ()
        try {
            for (let i = 0; i < this.regExpArrCount; i++) {
                // var data = "hello world xdhdusjbcjdcbdj vnkdvnfknvfkvnvfkn cvb djdvjbvfjbvjvfb"
                var matchData = this.uniqueArr(data.match(this.regExpArr[i]));
                if (matchData && matchData.length > 0) {
                    this.alertSystem(matchData, data, _id);

                    //call another function 
                } else {
                    logger.info('Nothing matched for id:' + _id);
                }
            }
        } catch (err) {
            logger.error(err.stack);
        }
    }

    alertSystem(matchData, alarmData, _id) {
        // this data will be inserted to elastic search

        logger.info("in alert system , the alarmData is: " + JSON.stringify(alarmData) + " " + "the _id is:" + _id)
        logger.info("the matchData is:" + JSON.stringify(matchData))



        elasticClient.search({
            index: 'log_monitoring_http',
            type: 'logs',
            body: {
                query: {
                    match: {_id: _id}
                }

            }
        }, function (err, data) {
            if (err) {
                return logger.error(err.stack)
            }

            if (data.hits.hits.length > 0) {
                var obj = {
                    'referenceID': _id,
                    'PRODUCT_NAME': data.hits.hits[0]._source.PRODUCT_NAME,
                    'LOCATION': data.hits.hits[0]._source.LOCATION,
                    'VENDOR_NAME': data.hits.hits[0]._source.VENDOR_NAME,
                    'IP_ADDRESS': data.hits.hits[0]._source.IP_ADDRESS,
                    'displayName': data.hits.hits[0]._source.displayName,
                    'TIME': moment().format('YYYY-MM-DD HH:mm:ss'),
                    'TYPE': data.hits.hits[0]._source.TYPE,
                    'content': matchData,
                    'apiOutput': alarmData,
                    'URL': data.hits.hits[0]._source.URL,
                    'pattern': data.hits.hits[0]._source.pattern,
                    'patternPreview':data.hits.hits[0]._source.patternPreview
                }

                logger.info("publishing this obj to redis:" + JSON.stringify(jsonPublishedToRedis))
                logger.info("inserting this obj into http_alarms:" + JSON.stringify(obj))
                var jsonPublishedToredis = {
                    'PRODUCT_NAME': data.hits.hits[0]._source.PRODUCT_NAME,
                    'LOCATION': data.hits.hits[0]._source.LOCATION,
                    'VENDOR_NAME': data.hits.hits[0]._source.VENDOR_NAME,
                    'IP_ADDRESS': data.hits.hits[0]._source.IP_ADDRESS,
                    'displayName': data.hits.hits[0]._source.displayName
                }
                redisSubmit.publish(config.redis.redisChannelForStackStorm, JSON.stringify(jsonPublishedToredis))
                elasticClient.index({
                    index: 'log_monitoring_http', //"log_monitoring"
                    type: 'alarms', //logs
                    body: obj
                }, function (err, response) {
                    if (err) {
                        return logger.error(err.stack)
                    }
                    logger.info("elastic search returns " + JSON.stringify(response))
                })
            } else
            {
                logger.error("the _id:" + _id + "is deleted from ES, cannot retriev product , vendor and location")
            }

        })

    }

    splitAndCreateRegExp(index) {
        try {
            var or = this.pattern[index].split('||');
            var orArr = [];
            var regExpString;
            for (let i = 0; i < or.length; i++) {
                var and = or[i].split('&&');
                var andArr = [];
                for (let j = 0; j < and.length; j++) {
                    andArr[j] = this.createRegExp(and[j].substr(0, 2), and[j].substr(2))
                }
                if (andArr.length > 1) {
                    orArr[i] = this.createRegExp('&&', andArr);
                } else if (andArr.length == 1) {
                    orArr[i] = String.raw`${andArr[0]}`;
                }
            }
            if (orArr.length > 1) {
                regExpString = this.createRegExp('||', orArr);
            } else if (orArr.length == 1) {
                regExpString = orArr[0];
            }
            return regExpString;
        } catch (err) {
            logger.error(err.stack);
        }
    }  //helper function
    createRegExp(operator, data) { //helper function used in splitAndCreateRegExp
        try {
            var returnStr;
            switch (operator) {
                case '==':
                    returnStr = String.raw`^(?=.*${data}.*\b).*`;
                    break;
                case '!=':
                    returnStr = String.raw`^(?!.*${data}.*\b).*`;
                    break;
                case '&&':
                    if (Array.isArray(data) && data.length > 1) {
                        returnStr = String.raw`${data.join('')}$`;
                    }
                    break;
                case '||':
                    if (Array.isArray(data) && data.length > 1) {
                        returnStr = String.raw`${data.join('|')}`;
                    }
                default:
                    break;
            }
            return returnStr;
        } catch (err) {
            logger.error(err.stack);
        }
    }
}
module.exports = Pattern;

