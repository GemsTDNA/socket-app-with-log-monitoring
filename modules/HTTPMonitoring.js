/**
 * Created by radhika on 10/24/2017.
 */
var request = require('request')
var path = require('path');

var Pattern = require(path.resolve(__dirname, "./Pattern.js"))


function setOptions(url, contentType, method, params, callback) {
    console.log("inside setOptions function" + url + " " + contentType + " " + method + " " + JSON.stringify(params))
    if (method == 'POST') {

        logger.info("POST!!")
        var obj = {
            url: url,
            //  headers :{'User-Agent': 'Mozilla/<version> (<system-information>) <platform> (<platform-details>) <extensions>'},
            contentType: "",
            method: method,
            // 'User-Agent': 'Mozilla/<version> (<system-information>) <platform> (<platform-details>) <extensions>'
        }

        if (params != undefined || params != null) {
            console.log(params[0])
            obj.body = params[0]
        }



        if (contentType == 'application/json') {
            obj.json = true
            obj.contentType = contentType

        }
        if (contentType == "" || contentType == 'text/plain') {

            obj.contentType = 'text/plain'


        }

        logger.info("the final formulated object in setOptions function is" + JSON.stringify(obj))
        return callback(null, obj)
    } else {
        var obj = {
            url: url,
            //  headers: {'content-type': "application/json"},
            //headers :{'User-Agent': 'Mozilla/<version> (<system-information>) <platform> (<platform-details>) <extensions>'},
            method: method,

            json: false
        }
        logger.info("inside method==GET condition of setOptions")

        if (params != undefined || params != null) {
            logger.info("the params recieved by GET method in setOptions is:" + JSON.stringify(params))
            obj.qs = params[0]

        }

        if (contentType == 'application/json') {
            obj.json = true
            obj.contentType = contentType

        }
        if (contentType == "" || contentType == 'text/plain') {

            obj.contentType = 'text/plain'


        }
        logger.info("the final formulated object in setOptions function is" + JSON.stringify(obj))
        return callback(null, obj)
    }

}










//    function setOptions(url, contentType, method, params, callback) {
//        console.log("inside setOptions function" + url + " " + contentType + " " + method + " " + JSON.stringify(params))
//
//
//     
//
//
//       if (method == 'GET') {
//            var obj = {
//                url: url,
//                headers: {'content-type': contentType},
//                method: method
//            }
//            logger.info("inside method==GET condition of setOptions")
//
//            if (params != undefined || params != null) {
//                logger.info("the params recieved by GET method in setOptions is:" + JSON.stringify(params))
//                obj.qs = params[0]
//            }
//
//
//            logger.info("the final formulated object in setOptions function is" + JSON.stringify(obj))
//            return callback(null, obj)
//        }
//        if (method == 'POST') {
//            var obj = {
//                url: url,
//                contentType: contentType,
//                method: method,
//            }
//
//            if (params != undefined || params != null) {
//                obj.body = params[0]
//            }
//
//            if (contentType == 'application/json') {
//                obj.json = true
//            }
//
//            return callback(null, obj)
//
//        }
//
//    }




function sendHTTPRequest(job_id, url, contentType, method, params, callback) {

    logger.info("sendHTTP function called from app.js file:" + job_id + " " + url + " " + contentType + " " + method + " " + JSON.stringify(params))

    setOptions(url, contentType, method, params, function (errSettingOptions, options) {
        logger.info("sendHTTP calls setOptions function")
        if (errSettingOptions) {
            logger.error("error from setOptions function:" + JSON.stringify(SettingOptions))
            return callback(errSettingOptions)
        } else {
            logger.info("the options returned by the setOptions function is:" + JSON.stringify(options))
            request(options, function (err, response, body) {
                logger.info("using the request module to call the API:" + options.url)
                if (err) {
                    //logger.error("error when calling the api using request in sendHTTPRequest" + JSON.stringify(err))
                    return callback(err)
                } else {
                    console.log("the request returns")
                    console.log(body)
                    return callback(null, body)
                }
            })
        }
    })

}


function patternMatching(pattern, caseSensitive, APIOutput, _id, contentType) {

    var data = {"caseSensitive": caseSensitive, "pattern": pattern}
    var pattern = new Pattern(data)
    var regExpArr = pattern.regExpFromArr()

//    if (contentType == 'application/json') {
//        APIOutput = JSON.stringify(APIOutput)
//    }
    APIOutput = JSON.stringify(APIOutput)
    pattern.searchPattern(APIOutput, _id)
}


exports.patternMatching = patternMatching
exports.sendHTTPRequest = sendHTTPRequest
























