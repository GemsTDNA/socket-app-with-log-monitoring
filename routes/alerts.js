var express = require('express');
var router = express.Router();

console.log("at api/alerts")
var alertHandler = require('../handlers/alerts')
//console.log(alertHandler)
//var authenticate = require("../libs/middleware").authenticate;
router.post('/alerts', alertHandler.getAlerts)
router.post('/getGraphDetails',alertHandler.sendAlerts)


module.exports = router;
