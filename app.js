var cors = require('cors');                                  //Get connectivity with Angular
var app = require('express')();
var server = require('http').Server(app);
global.io = require('socket.io')(server);
var bodyParser = require('body-parser');                           //For accesing body in the JSON object
var request = require('request');
var config = require('config');
var moment = require('moment')
var Agenda = require('agenda'); // for HTTPMonitoring
var path = require('path')



global.agenda = new Agenda({db: {address: config.monitoring.mongoURL, collection: config.monitoring.collection}});


var errorLogger = require(path.join(__dirname, 'libs', 'logger')).errorLogger;
var fileLogger = require(path.join(__dirname, 'libs', 'logger')).fileLogger;
var HTTPLogger = require(path.join(__dirname, 'libs', 'logger')).HTTPLogger
var logger = require(path.join(__dirname, 'libs', 'logger')).logger


var redis = require('redis');
var elasticsearch = require('elasticsearch');

var redisClient = redis.createClient(config.redis.port, config.redis.host);
var redisSubmit = redis.createClient(config.redis.port, config.redis.host);

//var redisAdapter= require('socket.io-redis');
//io.adapter(redisAdapter({ host: '172.16.23.27', port: 6379 }));

const Influx = require('influx');
const influx = new Influx.InfluxDB({
    host: config.influx.host, //'10.225.253.132',
    database: config.influx.db
});

global.elasticClient = new elasticsearch.Client({
    host: config.EsConfig.hosts,
    log: 'info'
});
var logs = require('./modules/logger');                      //Logger Init
var socketio = require('./modules/socketio');
var configAlert = require('./modules/configAlert');
var inventoryUtilisation = require('./modules/inventoryUtilisation');
var gemsTrail = require('./modules/gemsTrail.js');
var kpiAlarms = require('./modules/KPIAlarms');
var nowconfer = require('./modules/nowconfer');
var fileMonitoring = require('./modules/fileMonitoring')
var HTTPMonitoringFunctions = require('./modules/HTTPMonitoring')
var moment = require('moment');
var sprintf = require("sprintf-js").sprintf,
        vsprintf = require("sprintf-js").vsprintf;

//setTimeout(inventoryUtilisation.sendInventoryUtilisation, 30000);
//app.use(MyLogger);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.text());

global.IPAndSocketSession = []
global.sessionAndIP = []
//{socketid:id ,IP:IPAddress}

agenda.define('schedule_http_request', function (job, done) {
    HTTPLogger.info("schedule_http_request called")
    HTTPLogger.info("the attributes recieved by agenda.define function are shown below")
    HTTPLogger.info(job.attrs.data)

    var job_id = job.attrs.data.job_id;
    var url = job.attrs.data.http_url
    var method = job.attrs.data.method
    var contentType = job.attrs.data.contentType
    var params = job.attrs.data.params
    var pattern = job.attrs.data.pattern
    var caseSensitive = job.attrs.data.caseSensitive


    HTTPMonitoringFunctions.sendHTTPRequest(job_id, url, contentType, method, params, function (err, data) {
        HTTPLogger.info("inside the request function called by agenda.define")
        if (err) {
            HTTPLogger.error("error from request:" + JSON.stringify(err))

        }

        HTTPLogger.info("the data from sendHttp request in agenda.define is" + JSON.stringify(data))
        //var pattern, caseSensitive, APIOutput, _id
        HTTPLogger.info("calling pattern matching from agenda.start")
        var arr = []
        arr.push(data)
        HTTPMonitoringFunctions.patternMatching(pattern, caseSensitive, arr, job_id, contentType)
    });
    done();
});

agenda.on('ready', function () {
    HTTPLogger.info("Initalized agenda")
    
   agenda.start();
});


//function modified to decrement level = previousLevel
redisClient.on("message", function (channel, message) {
    logger.silly("Message received from redis : " + message);
    //	socketio.sendToRooms(io,message);
    var jsonObj = JSON.parse(message);
    jsonObj.gems = "Correctable";
    jsonObjpreviousLevel = "OK";
    logger.info("Product Level : " + jsonObj.level);
    redisSubmit.HINCRBY(jsonObj.TYPE + '_' + jsonObj.PRODUCT_NAME, jsonObj.level, 1, function (err, resp) {
        if (err) {
            logger.error("Got error while incrementing Redis Hash" + err);
        }
    });
    //decrementing the 'previousLevel' value for counter  of the given product
    redisSubmit.HINCRBY(jsonObj.TYPE + '_' + jsonObj.PRODUCT_NAME, jsonObj.previousLevel, -1, function (errWhileDecrementing, respAfterDecrement) {
        if (errWhileDecrementing) {
            logger.error("Got error while decrementing Redis Hash" + err);
        }
    });
    redisSubmit.HGETALL(jsonObj.TYPE + '_' + jsonObj.PRODUCT_NAME, function (err, resp) {
        if (err) {
            logger.error(err);
        } else {
            var tileJson = {
                "product": jsonObj.PRODUCT_NAME,
                "key": jsonObj.TYPE,
                '_id': jsonObj.PRODUCT_NAME + jsonObj.TYPE,
                "majorIssues": resp.WARNING,
                "criticalIssues": resp.CRITICAL
            };
            //logger.info("Data in HGETALL : " + JSON.stringify(tileJson));
            io.to(jsonObj.PRODUCT_NAME).emit("message_from_subscribed", tileJson);
            elasticClient.index({
                index: 'alertsfromsocketio',
                type: 'logs',
                body: jsonObj
            }, function (error, response) {
                if (err) {
                    logger.error("Error while inserting data into ES : " + err);
                } else {
                    //logger.silly(JSON.stringify(response));
                    jsonObj['_id'] = response['_id']; //Required Uniq id for index id
                    logger.info("ID of the doc inserted to alertsfromsocketio is:" + response['_id'])
                    if (jsonObj.level == "CRITICAL") {
                        logger.silly("Sending to UI : " + JSON.stringify(jsonObj));
                        io.to(jsonObj.PRODUCT_NAME).emit("alertsWindow", jsonObj);
                    }
                    if (jsonObj.TYPE == 'monitor') {

                        //insert data to ES if type:MONITOR
                        // check if ID (IP_+alarm_name)exists
                        // yes => get the level and previous ID
                        // compare with the level received now
                        // higher => add escalate into the old document  escalated:CRITICAL
                        // lower=> add cleared into the old document  cleared:CRITICAL
                        // now add new document


                        // previous level is recieved via json when type is KPI .
                        // if(jsonObj.previousLevel == undefined)
                        // {
                        //     jsonObj.previousLevel = 'MAJOR'
                        // }
                        var insertIDAfterAddingEscaltedOrCleared = response['_id']
                        logger.silly("inserting data to ES under the index monitor")
                        var ID = jsonObj.IP_ADDRESS + '_' + jsonObj.ALARM_NAME
                        var index = 'monitor'
                        var type = 'logs'
                        logger.info('function search called with params' + ID + ',' + index + ',' + type)
                        search(ID, index, type, function (errSearchingDocument, documentFound) {

                            if (errSearchingDocument) {
                                logger.error(errSearchingDocument)
                            }

                            if (documentFound.success != 0) {

                                // document exists
                                var previousID = documentFound.document.hits.hits[0]._source.reference_doc_id
                                logger.info("the document to be updated has an ID:" + previousID)
                                var index = 'alertsfromsocketio'
                                var type = 'logs'
                                var level = jsonObj.level
                                logger.info("current level is" + level)
                                var alertsFromSocketIoLevel = documentFound.document.hits.hits[0]._source.level
                                logger.info("level from alertsfromsocketio index's previous doc is" + alertsFromSocketIoLevel)
                                var levelData = {"CRITICAL": 4, "MAJOR": 3, "INFO": 1, "OK": 1, "WARNING": 1}
                                var alarmTime = jsonObj.time
                                var indexForDocumentWithIPAlarmNameAsIndex = 'monitor'
                                var typeForDocumentWithIPAlarmNameAsIndex = 'logs'
                                if (levelData[alertsFromSocketIoLevel] < levelData[level]) {

                                    var escalated = 'escalated' + ':' + level
                                    logger.info("update document with values" + " " + alarmTime + " " + escalated)
                                    updateAlertsFromSocketIO(insertIDAfterAddingEscaltedOrCleared, previousID, index, type, alarmTime, escalated, function (errUpdatingDocument, documentUpdated) {
                                        if (errUpdatingDocument) {
                                            logger.error(errUpdatingDocument)
                                        }
                                        jsonObj.reference_doc_id = jsonObj['_id']
                                        jsonObj.previous_level = alertsFromSocketIoLevel
                                        //jsonObj.previous_level = jsonObj.previousLevel
                                        updateDocumentWithIPAlarmNameAsIndex(ID, index, type, jsonObj, function (updateErr, updated) {
                                            if (updateErr) {
                                                logger.error(updateErr)
                                            }
                                            logger.info("all documents are updated")
                                        })
                                    })
                                } else if (levelData[alertsFromSocketIoLevel] > levelData[level]) {
                                    var cleared = 'cleared' + ':' + level
                                    logger.info("update document with values" + " " + alarmTime + " " + cleared)
                                    updateAlertsFromSocketIO(insertIDAfterAddingEscaltedOrCleared, previousID, index, type, alarmTime, cleared, function (errUpdatingDocument, documentUpdated) {
                                        if (errUpdatingDocument) {
                                            logger.error(errUpdatingDocument)
                                        }
                                        jsonObj.reference_doc_id = jsonObj['_id']
                                        jsonObj.previous_level = alertsFromSocketIoLevel
                                        // jsonObj.previous_level = jsonObj.previousLevel
                                        updateDocumentWithIPAlarmNameAsIndex(ID, indexForDocumentWithIPAlarmNameAsIndex, typeForDocumentWithIPAlarmNameAsIndex, jsonObj, function (updateErr, updated) {
                                            if (updateErr) {
                                                logger.error(updateErr)
                                            }
                                            logger.info("all documents are updated")
                                        })
                                    })
                                } else {
                                    logger.info("the levels are equal , no need to add escalated or cleared ")
                                    jsonObj.reference_doc_id = jsonObj['_id']
                                    jsonObj.previous_level = alertsFromSocketIoLevel
                                    //jsonObj.previous_level = jsonObj.previousLevel
                                    updateDocumentWithIPAlarmNameAsIndex(ID, indexForDocumentWithIPAlarmNameAsIndex, typeForDocumentWithIPAlarmNameAsIndex, jsonObj, function (updateErr, updated) {
                                        if (updateErr) {
                                            logger.error(updateErr)
                                        }
                                        logger.info("all documents are updated")
                                    })
                                }
                            } else {
                                //insert new document
                                logger.info("No document exists, therefore inserting a new document to the index:" + 'monitor')
                                jsonObj.reference_doc_id = jsonObj['_id']
                                delete jsonObj._id
                                elasticClient.index({
                                    index: 'monitor',
                                    type: 'logs',
                                    id: ID,
                                    body: jsonObj
                                }, function (errWhileInserting, responseAfterInsert) {
                                    if (errWhileInserting) {
                                        logger.error("Error while inserting data into ES : " + errWhileInserting);
                                    }

                                })
                            }

                        })

                    }

                }
            });
        }
    });
});


function updateAlertsFromSocketIO(nextDocumentID, ID, index, type, alarm, status, callback) {
    logger.info("update alertsFromSocketio index with the ID:" + ID + " " + "index:" + index + " " + "alarm status:" + status + " " + "alarm time" + alarm)
    elasticClient.update({
        index: index,
        type: type,
        id: ID,
        body: {
            // put the partial document under the `doc` key
            doc: {
                ALARM_TIME: alarm,
                ALARM_STATUS: status,
                next_doc_id: nextDocumentID
            }
        }
    }, function (error, response) {
        if (error) {
            return callback(error)
        }
        return callback(null, response)
    })

}


function updateDocumentWithIPAlarmNameAsIndex(ID, index, type, obj, callback) {
    delete obj._id
    delete obj.previousLevel
    var index = 'monitor' // check
    logger.info("update documentWithIPAlarmNameAsIndex with the index:" + index + "" + " " + "with the ID:" + ID + " " + "type:" + type + " " + "body:" + JSON.stringify(obj))
    elasticClient.index({
        index: index,
        type: type,
        id: ID,
        body: obj
    }, function (err, data) {
        if (err) {
            return callback(err)
        }
        return callback(null, data)
    })
}


function search(ID, index, type, callback) {
    elasticClient.search({
        index: index,
        type: type,
        body: {
            query: {
                match: {
                    _id: ID
                }
            }
        }
    }, function (err, data) {
        if (err) {
            var jsonObj = {"success": "0"}
            return callback(null, jsonObj)


        }
        if (data.hits.total > 0) {
            var jsonObj = {"success": "1", "document": data}
            return callback(null, jsonObj)
        } else {
            var jsonObj = {"success": "0"}
            return callback(null, jsonObj)
        }
    })
}


function fetchHashes(IP, callback) {
    redisSubmit.hget(IP, "status", function (err, res) {
        if (err) {
            return callback(err)
        }
        return callback(null, res)
    })
}

function fetchAllkeysInHash(IP, callback) {
    redisSubmit.hgetall(IP, function (err, res) {
        if (err) {
            return callback(err)
        }
        return callback(null, res)

    })
}

function determineIfkeyExistsInRedis(IP, callback) {

    redisSubmit.exists(IP, function (err, res) {
        if (err) {
            return callback(err)
        }
        return callback(null, res)
    })
}

function editHashStatus(IP, statusMessage, callback) {
    redisSubmit.hset(IP, "status", statusMessage, function (err, res) {
        if (err) {
            return callback(err)
        }
        return callback(null, res)
    })
}

function fetchAllElementsOfset(IP, callback) {
    redisSubmit.smembers(IP, function (err, res) {
        if (err) {
            return callback(err)
        }
        return callback(null, res)
    })
}

function getDataBasedOnIDs(listOfIDs, callback) {
    elasticClient.search({
        index: "log_monitoring",
        type: "logs",
        body: {
            query: {
                terms: {
                    "_id": listOfIDs
                }
            }
        }
    }, function (err, response) {
        if (err) {
            logger.error(err)
        }

        var results = []
        response.hits.hits.forEach(function (item) {
            var source = item._source
            source._id = item._id
            results.push(source)
        })
        return callback(null, results)
    })
}

function deleteRedisHash(IP, callback) {
    redisSubmit.del(IP, function (err, res) {
        if (err) {
            return callback(err)
        }
        return callback(null, res)
    })
}

function fetchPLVFromLogMonitoringIndex(_id, callback) {
//    var logger = monitoringLogger
    elasticClient.search({
        index: "log_monitoring",
        type: "logs",
        body: {
            query: {
                match: {_id: _id}
            }
        }

    }, function (err, response) {
        if (err) {
            // console.log(err)
            return callback(err)
        }
        logger.info("fetchPLVFromMonitoring returns:" + JSON.stringify(response))

        return callback(null, response)

    })
}

function fetchAllDataES(index, type, callback) {
    logger.info("fetching all data in the ES index:" + index + " " + "with type:" + type)
    elasticClient.search({
        index: index,
        type: type,
        body: {
            query: {
                match_all: {}
            }
        }

    }, function (err, response) {
        if (err) {
            // console.log(err)
            return callback(err)
        }
        logger.info("fetchALLDataEs returns:" + JSON.stringify(response))

        return callback(null, response)

    })
}

function insertAlarmsFromLogMonitoring(obj, callback) {
//    var logger = monitoringLogger
    // once sample data is provided , create mappings for the index .
    //index doesn't exists yet
    elasticClient.index({
        index: "log_monitoring",
        type: "alarms",
        body: obj

    }, function (err, response) {
        if (err) {
            // console.log(err)
            return callback(err)
        }
        //logger.info("addAuditDetails for " + data.product + " " + data.vendor + " " + data.location + " " + "with IP" + " " + data.IP + " " + "has been added to userAuditMonitoring")
        return callback(null, {"status": 1})


    })
}

redisClient.subscribe("products");

app.use(cors())
app.post('/nowconfer', [nowconfer.emitdata]);
app.get('/inventoryUtilisation', [inventoryUtilisation.sendInventoryUtilisation]);
app.post('/configAlerts', [configAlert.configAlerts]);
app.post('/api/alerts', [socketio.parseAlarm]);
app.post('/gemsTrailAnomoly', [gemsTrail.gemsTrailAnomoly]);
app.post('/gemsTrailAssociation', [gemsTrail.gemsTrailAssociation]);
app.post('/gemsTrailRuleEngine', [gemsTrail.gemsTrailRuleEngine]);
app.post('/api/kpi_alarms', [kpiAlarms.kpi_alarms])
app.post('/api/gemsActions', [kpiAlarms.gemsActions])


app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');
});


/*changes for log monitoring*/


app.post('/addMonitoringData', [fileMonitoring.addLogMonitoringData])
app.post('/editPattern', [fileMonitoring.editPattern])
app.post('/stopWatchingFile', [fileMonitoring.stopWatchingFile])
app.get('/listData', [fileMonitoring.listData])
app.get('/getMonitoringAlarms', [fileMonitoring.getMonitoringAlarms])
app.get('/deleteMonitoringData', [fileMonitoring.deleteDataFromMonitoring])
app.post('/addMonitoringDataHTTP', [fileMonitoring.addMonitoringDataWithTypeHTTP])
app.post('/validateWhenAddingLogMonitoringData', [fileMonitoring.validateWhenAddingLogMonitoringData])
app.post('/validationForMonitoringDataHTTP', [fileMonitoring.validationForMonitoringDataHTTP])
app.post('/addMonitoringDataForCounters', [fileMonitoring.addMonitoringDataForCounters])
app.post('/deleteDataForCounterMonitoring', [fileMonitoring.deleteDataForCounterMonitoring])
app.post('/editCounterMonitoringData', [fileMonitoring.editCounterMonitoringData])
app.post('/validateWhenAddingCounterData', [fileMonitoring.validateWhenAddingCounterData])
app.post('/deleteDataForHTTPMonitoring', [fileMonitoring.deleteDataForHTTPMonitoring])
app.post('/updatePatternForHTTPMonitoring', [fileMonitoring.updatePatternForHTTPMonitoring])


io.on('connection', function (socket) {
    function sendHeartbeat() {
        setTimeout(sendHeartbeat, 8000);
        socket.emit('ping', {beat: 1});
    }

    setTimeout(sendHeartbeat, 8000)

    //setTimeout(sendInventoryUtilisation,3);


    socket.on('userName', function (data) {
        var socketid = socket.socketid
        logger.info("THE USERNAME IS" + data)
        request.post('http://172.16.23.27:3001/getProducts', {
            json: {
                UserName: data
            }
        }, function (error, response, body) {
            logger.info("INSIDE REQUEST")
            if (error) {
                logger.error(error);
            } else {
                logger.info("the user has access to" + JSON.stringify(body))
                var array = ["monitor", "KPI", "control", "performance"];
                body.forEach(function (room) {
                    array.forEach(function (type) {
                        //logger.silly("Joining the room : " + room);
                        redisSubmit.HGETALL(type + '_' + room, function (err, resp) {
                            if (err) {
                                logger.error(err);
                            } else {
                                var tileJson = {
                                    "product": room,
                                    "key": type,
                                    '_id': room + type,
                                    "majorIssues": resp.WARNING,
                                    "criticalIssues": resp.CRITICAL
                                };
                                //logger.info("Data in HGETALL : " + JSON.stringify(tileJson));
                                // io.to(room).emit("message_from_subscribed", tileJson);
                                io.to(socket.id).emit("message_from_subscribed", tileJson);
                            }
                        });
                        elasticClient.search({
                            index: 'alertsfromsocketio',
                            body: {
                                size: 25,
                                query: {
                                    "bool": {
                                        "must": [
                                            {"range": {"time": {"gte": "now-10h", "lte": "now"}}},
                                            {"term": {"PRODUCT_NAME": room}},
                                            {"term": {"level": "CRITICAL"}}
                                        ]
                                    }
                                }
                            }
                        }, function (err, data) {
                            if (err) {
                                logger.error("error querying elasticSearch" + JSON.stringify(err))
                            } else {
                                data.hits.hits.forEach(function (record) {
                                    var jsonObj = record['_source'];
                                    jsonObj['_id'] = record['_id'];
                                    //io.to(room).emit("alertsWindow", jsonObj);
                                    io.to(socket.id).emit("alertsWindow", jsonObj)
                                })
                            }
                        });
                    })
                    elasticClient.search({
                        index: 'kpi_alarms',
                        body: {
                            size: 25,
                            query: {
                                "bool": {
                                    "must": [
                                        {"range": {"time": {"gte": "now-10h", "lte": "now"}}},
                                        {"term": {"PRODUCT_NAME": room}},
                                        {"term": {"level": "CRITICAL"}}
                                    ]
                                }
                            }
                        }
                    }, function (err, data) {
                        if (err) {
                            logger.error("error querying elasticSearch" + JSON.stringify(err))
                        } else {
                            data.hits.hits.forEach(function (record) {
                                var jsonObj = record['_source'];
                                jsonObj['_id'] = record['_id'];
                                //io.to(room).emit("alertsWindow", jsonObj);
                                io.to(socket.id).emit("alertsWindow", jsonObj)
                            })
                        }
                    });

                    elasticClient.search({
                        index: 'gemstrail',
                        type: 'association',
                        body: {
                            size: 25,
                            query: {
                                "bool": {
                                    "must": [
                                        {"range": {"time": {"gte": "now-10h", "lte": "now"}}},
                                        {"term": {"PRODUCT_NAME": room}}
                                    ]
                                }
                            }
                        }
                    }, function (err, data) {
                        logger.info("GEMS : " + JSON.stringify(data));
                        if (err) {
                            logger.error("error querying elasticSearch" + JSON.stringify(err))
                        } else {
                            data.hits.hits.forEach(function (record) {
                                var jsonObj = record['_source'];
                                jsonObj['_id'] = record['_id'];
                                console.log("EMITTING DATA TO GEMS TRAIL ")
                                console.log(jsonObj)
                                logger.info("Emmitting to GEMS trail" + jsonObj);
                                //io.to(room).emit("gemstrail", jsonObj);
                                io.to(socket.id).emit("gemstrail", jsonObj)
                            })
                        }
                    });
                    elasticClient.search({
                        index: 'configdiff,useraudit3',
                        type: 'logs',
                        body: {
                            size: 25,
                            sort: [{"time": {"order": "desc"}}],
                            query: {
                                "bool": {
                                    "must": [
                                        {"range": {"time": {"gte": "now-1M", "lte": "now"}}},
                                        {"term": {"PRODUCT_NAME": room}}
                                    ]
                                }
                            }
                        }
                    }, function (err, data) {
                        logger.info("GEMS : " + JSON.stringify(data));
                        if (err) {
                            logger.error("error querying elasticSearch" + JSON.stringify(err))
                        } else {
                            data.hits.hits.forEach(function (record) {
                                var jsonObj = record['_source'];
                                jsonObj['_id'] = record['_id'];
                                logger.info("Emmitting to Config trail" + jsonObj);
                                //io.to(room).emit("configtrail", jsonObj);
                                io.to(socket.id).emit("configtrail", jsonObj)
                            })
                        }
                    });
                    socket.join(room);
                })
            }
        })
    });

    socket.on('disconnect', function () {
        //   logger.info("DISCONNECT");
        var socketIDOfDisconnectingClient = socket.id
        // logger.info("the socketid of the disconnecting client is" + socket.id)

//        logger.info("the sessionAndIP array is shown below")
//        logger.info(sessionAndIP)
//        logger.info("the IP and sessionArray is ")
//        logger.info(IPAndSocketSession)
        sessionAndIP.forEach(function (eachObj, firstLoopIndex, firstArr) {
            if (eachObj[socketIDOfDisconnectingClient]) {
                var IP = eachObj[socketIDOfDisconnectingClient]
                logger.info("Deleting object with the IP:" + IP)
                logger.info("the client IP address obtained from socket is " + IP)
                logger.info("deleting  redis hash and sets with the IP" + IP)
                var redisSet = IP + "_set"
                redisSubmit.del(redisSet)
                redisSubmit.del(IP)
                IPAndSocketSession.forEach(function (obj, index, arr) {
                    if (obj[IP]) {
                        logger.info("deleting from IPAndSocketSession" + JSON.stringify(obj))
                        arr.splice(index, 1)
                    }
                })
                firstArr.splice(firstLoopIndex, 1)
            }


        })
//        logger.info("the sessionIP array after disconnect and deletion is as follows")
//        logger.info(sessionAndIP)


    });

    socket.on('error', function (errData) {
        logger.error("Error in socket connection" + errData);
    });

    socket.on('alarmRaised', function (obj) {
        // var obj = {
        //     "id": "AV8AVE8pDsP3McqjTnLQ",
        //     "matchData":[{"level":"info","message":"# Python syntax","timestamp":"2017-10-12T08:46:52.382Z"}, {"level":"info","message":"# Python syntax","timestamp":"2017-10-12T08:47:00.391Z"}]
        //     ,
        //     "searchString": undefined
        //
        // }
        var date = moment().format('YYYY-MM-DD HH:mm:ss')
        fileLogger.info("the current date is" + date)
        fileLogger.info("alarmRaised event emitted with the following data")
        fileLogger.info(obj)
        fileLogger.info("object's id in alarmRaised event is" + obj.id)
        fileLogger.info('objects matched pattern in alarmRaised event is' + obj.matchData)
        fileLogger.info("objects search string in alarmRaised event is" + obj.searchString)
        fileLogger.info("the filename is" + obj.fileName)
        fetchPLVFromLogMonitoringIndex(obj.id, function (err, data) {
            if (err) {
                fileLogger.error(err)
            }
            fileLogger.info("fetchPLVfromLog Monitoring returns data that is shown below")
            fileLogger.info(data)
            if (data.hits.hits.length == 0) {
                fileLogger.info("the id sent in raisedAlarm event doesn't exist or doesn't have any data:" + obj.id)
            } else {
                var EsData = data.hits.hits[0]
                fileLogger.info("the data from ES for alarmRaised event is:" + JSON.stringify(EsData))


                if (obj.fileName) {
                    var appendToPath = EsData._source.path + obj.fileName
                } else {
                    var appendToPath = EsData._source.path
                }
                var jsonObj = {
                    "PRODUCT_NAME": EsData._source.PRODUCT_NAME,
                    "LOCATION": EsData._source.LOCATION,
                    "VENDOR_NAME": EsData._source.VENDOR_NAME,
                    "IP_ADDRESS": EsData._source.IP_ADDRESS,
                    "TYPE": EsData._source.TYPE,
                    "path": appendToPath,
                    "content": obj.searchString.toString(),
                    "matchedPattern": obj.matchData.toString(),
                    "TIME": date,
                    "displayName": EsData._source.displayName,
                    "patternPreview":EsData._source.patternPreview
                    

                }
                var jsonToEmit = {
                    "PRODUCT_NAME": EsData._source.PRODUCT_NAME,
                    "LOCATION": EsData._source.LOCATION,
                    "VENDOR_NAME": EsData._source.VENDOR_NAME,
                    "IP_ADDRESS": EsData._source.IP_ADDRESS,
                    //   "TYPE": EsData._source.TYPE,
                    // "TIME": date,
                    "displayName": EsData._source.displayName

                }
                fileLogger.info("the following data is sent to redis channel called monitoring")
                fileLogger.info(jsonToEmit)
                redisSubmit.publish(config.redis.redisChannelForStackStorm, JSON.stringify(jsonToEmit));

                fileLogger.info("inserting the following data into ES log_monitoring_alarms")
                fileLogger.info(jsonObj)
                //to emit to a room incase the python app requires 
                ////io.to(room).emit("eventName", jsonObj);

                insertAlarmsFromLogMonitoring(jsonObj, function (errInsertingData, dataInserted) {
                    if (errInsertingData) {
                        fileLogger.error(errInsertingData)
                    }
                })
            }

        })

    })

    /* python app emits an event , using which the server joins the client to a room
     alarmRaised objects are emitted to the room.
     */


    // socket.on('event',function(){
    //     socket.join('room')
    // })

    socket.on('logMonitoringSession', function (obj) {
        fileLogger.info("LOG MONITORING SESSION event received:" + JSON.stringify(obj))
        fileLogger.info("the IP address of the connecting client is:" + socket.client.conn.remoteAddress)
        var IPFromSocket = socket.client.conn.remoteAddress

        //TO-DO remove in production after testing to determine the values recieved for IPSocket variable
        if (IPFromSocket == '::1') {
            var IPAddress = '127.0.0.1'
        } else {
            var IPAddress = IPFromSocket.split(':')[3]
        }
        fileLogger.info("the IPAddress after splitting is:" + IPAddress)

        //::ffff:172.16.3.178

        var id = socket.id

        fileLogger.info("the sessionAndIP array BEFORE addition is shown below")
        fileLogger.info(sessionAndIP)
        var obj1 = {}
        obj1[IPAddress] = id
        IPAndSocketSession.push(obj1)
        var obj2 = {}
        obj2[id] = IPAddress
        sessionAndIP.push(obj2)
        fileLogger.info("the sessionAndIP array AFTER addition is shown below")
        fileLogger.info(sessionAndIP)
        fileLogger.info("the IPsession array is shown below")
        fileLogger.info(IPAndSocketSession)
        console.log("the IP ADDRESS IS" + IPAddress)

        determineIfkeyExistsInRedis(IPAddress, function (errWhileCheckingIfKeyExists, keyInformation) {
            fileLogger.info("checking if hash:" + IPAddress + "exists in redis")
            if (errWhileCheckingIfKeyExists) {
                fileLogger.error(errWhileCheckingIfKeyExists)
                //errorLogger.error(errWhileCheckingIfKeyExists)
            }
            if (keyInformation) {
                fileLogger.info(" hash:" + IPAddress + "found in redis")
                fetchHashes(IPAddress, function (errFetchingHash, hashData) {
                    if (errFetchingHash) {
                        fileLogger.error(errFetchingHash)
                    }
                    if (hashData == 'awaitingConnection') {
                        console.log("status is awaitingConnection - changing to connected")
                        editHashStatus(IPAddress, "connected", function (errEditingHash, editedHashData) {
                            if (errEditingHash) {
                                fileLogger.error(errEditingHash)
                            }
                            // get id's from sets and query elastic search
                            checkIfAddMonitoringAPICalled(IPAddress, function (errWithAddMonitoringDataFunction, ESData) {
                                if (errWithAddMonitoringDataFunction) {
                                    fileLogger.error(errWithAddMonitoringDataFunction)
                                } else {
                                    ESData.forEach(function (eachObj) {
                                        eachObj.id = eachObj._id
                                        delete eachObj._id
                                        socket.emit("startProcessing", eachObj);
                                    })
                                }

                            })
                            checkIfAddPatternAPICalled(IPAddress, function (errWithAddPatternFunction, redisHashData) {
                                if (errWithAddPatternFunction) {
                                    fileLogger.error(errWithAddPatternFunction)
                                }
                                // console.log(typeof redisHashData)
                                var arr = []
                                arr.push(redisHashData)
                                arr.forEach(function (obj) {
                                    var _idFromEs = Object.keys(obj)
                                    var patternFromObj = obj[_idFromEs]
                                    socket.emit("addPattern", {id: _idFromEs, "pattern": patternFromObj})
                                })

                            })

                            checkIfStopProcessingAPICalled(IPAddress, function (errWithStopProcessingAPI, data) {
                                if (errWithStopProcessingAPI) {
                                    logger.error(errWithStopProcessingAPI)
                                }
                                fileLogger.info("the data in redis set:" + IPAddress + "_stop_processing is shown below")
                                fileLogger.info(data)

                                data.forEach(function (eachObj) {
                                    socket.emit("stopProcessing", eachObj);
                                })
                            })


                        })
                    }
                    if (hashData == 'connected') {
                        logger.info("hashData is CONNECTED")
                        //  console.log("status is connected - changing to reconnected")
                        //  //this has been removed as per discussion with Prem on 27/11/2017. There is no need for the reconnected status as the script ensure that more than one salt process is not run
//                        editHashStatus(IPAddress, "reconnected", function (errEditingHash, editedHashData) {
//                            if (errEditingHash) {
//                                fileLogger.error(errEditingHash)
//                            }
//                        })
                        sendAllData('log_monitoring', 'logs', function (errOnReconnectStatus, allESData) {
                            if (errOnReconnectStatus) {
                                return next(errOnReconnectStatus)
                            }


                            logger.info('on connect function emits the following Data:' + JSON.stringify(allESData))
                            if (allESData.length > 0) {
                                allESData.forEach(function (itemToEmit) {
                                    logger.info("each item emitted")
                                    logger.info(itemToEmit)
                                    socket.emit('startProcessing', itemToEmit)
                                })
                            }



                        })
                    }
//                    if (hashData == 'reconnected') {
//                        //do nothing
//
//                        fileLogger.info("status is reconnected")
//
//
//                    }

                })
            } else {
                fileLogger.info("hash for the IP:" + IPAddress + " " + " is not found")
                fileLogger.info("creating redis set:" + IPAddress)

                createRedisHash(IPAddress, function (errWhileCreatingRedisHash, redisHashCreated) {
                    if (errWhileCreatingRedisHash) {
                        fileLogger.error(errWhileCreatingRedisHash)
                    }

                })


            }
        })
    })

});




function checkIfAddMonitoringAPICalled(IPAddress, callback) {
    fetchAllElementsOfset(IPAddress + "_set", function (errFindingSetData, setDataFound) {
        if (errFindingSetData) {
            return callback(errFindingSetData)
        }
        fileLogger.info("fetching data from redis set called:" + IPAddress)
        getDataBasedOnIDs(setDataFound, function (errFindingIDsInES, ESData) {
            if (errFindingIDsInES) {
                return callback(errFindingIDsInES)
            }
            console.log(ESData)
            return callback(null, ESData)


        })
    })
}


function checkIfAddPatternAPICalled(IPAddress, callback) {
    determineIfkeyExistsInRedis(IPAddress + "_add_pattern", function (keyErr, keyFound) {
        if (keyErr) {
            return callback(keyErr)
        }
        if (keyFound) {
            fileLogger.info("fetching data when add pattern API was called")
            fetchAllkeysInHash(IPAddress + "_add_pattern", function (hashErr, hashFound) {
                fileLogger.info("the redis hash" + IPAddress + "_add_pattern returns" + JSON.stringify(hashFound))
                return callback(null, hashFound)
            })
        } else {
            fileLogger.info("the redis hash" + IPAddress + "_add_pattern is not found")
        }
    })
}


function checkIfStopProcessingAPICalled(IPAddress, callback) {
    determineIfkeyExistsInRedis(IPAddress + "_stop_processing", function (keyErr, keyFound) {
        if (keyErr) {
            return callback(keyErr)
        }
        if (keyFound) {
            fileLogger.info("fetching data when stop processing API was called")
            fetchAllElementsOfset(IPAddress + "_stop_processing", function (errFindingSetData, setDataFound) {
                if (errFindingSetData) {
                    return callback(errFindingSetData)
                }
                fileLogger.info("fetching data from redis set called:" + IPAddress)
                return callback(null, setDataFound)
            })

        }
    })
}

function createRedisHash(IP, callback) {
    redisSubmit.hset(IP, "status", "connected", function (err, res) {
        if (err) {
            return callback(err)
        }
        //redisSubmit.expire(IP,300)
        return callback(null, res)
    });
}
function sendAllData(index, type, callback) {
    logger.info("inside sendALLData function ")
    var arr = []
    fetchAllDataES(index, type, function (err, data) {
        if (err) {
            return callback(err)
        }
        data.hits.hits.forEach(function (item) {
            if (item._source.watchDirectory == true) {

                var jsonObj = {
                    //  "fileName": fileName,
                    "path": item._source.path,
                    "IP_ADDRESS": item._source.IP_ADDRESS,
                    "pattern": item._source.pattern,
                    "caseSensitive": item._source.caseSensitive,
                    "TYPE": item._source.type,
                    "watchDirectory": item._source.watchDirectory,
                    "riseAlarm": item._source.riseAlarm,
                    //"extension":  item._source.filename.split('.')[1],
                    "processing": 1,
                    "PRODUCT_NAME": item._source.PRODUCT_NAME,
                    "LOCATION": item._source.LOCATION,
                    "VENDOR_NAME": item._source.VENDOR_NAME,
                    "id": item._id

                }
                var extension = item._source.filename.split('.')[1]
                if (extension != undefined) {
                    jsonObj.extension = extension
                }
            } else
            {

                var jsonObj = {

                    "path": item._source.path,
                    "fileName": item._source.fileName,
                    "IP_ADDRESS": item._source.IP_ADDRESS,
                    "pattern": item._source.pattern,
                    "caseSensitive": item._source.caseSensitive,
                    "TYPE": item._source.type,
                    "watchDirectory": item._source.watchDirectory,
                    "riseAlarm": item._source.riseAlarm,
                    //"extension":  item._source.filename.split('.')[1],
                    "processing": 1,
                    "PRODUCT_NAME": item._source.PRODUCT_NAME,
                    "LOCATION": item._source.LOCATION,
                    "VENDOR_NAME": item._source.VENDOR_NAME,
                    "id": item._id

                }
            }

            arr.push(jsonObj)

        })
        logger.info("sendALLdata returns the following:" + JSON.stringify(arr))
        logger.info(arr)
        return callback(null, arr)

    })
}

if (app.get('env') === 'development') {
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
    app.use(function (err, req, res, next) {
        var responseJson = {};
        responseJson.status = 0;
        responseJson.msg = err.message;
        responseJson.errorName = err.name;
        res.status(err.status || 500);
        if (err.path) {
            responseJson.path = err.path
        }
        // res.status(500)
        res.json(responseJson);
        res.end();
        return;
    });
}




//
// production error handler
// no stacktraces leaked to user



app.use(function (err, req, res, next) {
    // errorLogger.error(err)
    var responseJson = {};
    responseJson.status = 0;
    //responseJson.stack = err.stack
    responseJson.msg = err.message;
    responseJson.errorName = err.name;
    res.status(err.status || 500);
    res.json(responseJson);
    res.end();
    return;
});

server.listen(3009, function () {
    logger.info('Local http : http://localhost:3009/');
});
