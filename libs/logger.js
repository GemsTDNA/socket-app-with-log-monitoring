/**
 * Created by Radhika on 11/9/2017.
 */
var winston = require('winston');
//var expressWinston = require('express-winston')
var path = require('path')
var dt = require('datetimejs');
var LogsDir = path.join(__dirname, '..', 'logs'); // current directory is lib

var counterMonitoringFileName = path.join(LogsDir, "counterMonitoring.log")
var HTTPMonitoringFileName = path.join(LogsDir, "HTTPMonitoring.log")
var fileMonitoringFileName = path.join(LogsDir, "FileMonitoring.log")
var loggerFileName = path.join(LogsDir, "logger.log")
var errLoggerFileName = path.join(LogsDir, "error.log")
var accessLoggerFileName = path.join(LogsDir, "access.log")
function myTimestamp() {
    var date = new Date();
    return dt.strftime(date, '%Y-%m-%d %H:%M:%S:%r');
    //  return new Date().toString();
};

var counterLoggerObj = new (winston.Logger)({
    transports: [
        new (winston.transports.Console)({level: 'info'}),
        new (winston.transports.File)({
            filename: counterMonitoringFileName,
            level: 'info',
            json: true,
            maxFiles: 2,
            maxsize: 1048576, //1MB
            timestamp: myTimestamp,
            colorize: true
        })
    ]
});


var HTTPLoggerObj = new (winston.Logger)({
    transports: [
        new (winston.transports.Console)({level: 'info'}),
        new (winston.transports.File)({
            filename: HTTPMonitoringFileName,
            level: 'info',
            json: true,
            maxFiles: 2,
            maxsize: 1048576,
            timestamp: myTimestamp,
            colorize: true
        })
    ]
});

var fileLoggerObj = new (winston.Logger)({
    transports: [
        new (winston.transports.Console)({level: 'info'}),
        new (winston.transports.File)({
            filename: fileMonitoringFileName,
            level: 'info',
            json: true,
            maxFiles: 2,
            maxsize: 1048576,
            timestamp: myTimestamp,
            colorize: true
        })
    ]
});

var loggerObj = new (winston.Logger)({
    transports: [
        new (winston.transports.Console)({level: 'info'}),
        new (winston.transports.File)({
            filename: loggerFileName,
            level: 'info',
            json: true,
            maxFiles: 2,
            maxsize: 1048576,
            timestamp: myTimestamp,
            colorize: true
        })
    ]
});
/*
 var errorLogObj = {
    transports: [
        new winston.transports.File({
            filename: errorFileName,
            maxsize: 1048576, //1MB
            maxFiles: 2,
            json: true,
            timestamp: myTimestamp,
            colorize: true,
            handleExceptions: true,
            exitOnError: false,
        })
    ],
    colorStatus: true,
    requestWhitelist: ['url', 'headers', 'method', 'httpVersion', 'originalUrl', 'query', 'body']
}


var errorLogger = new expressWinston.errorLogger(errorLogObj);
 */
var errLoggerObj =  new (winston.Logger)({
    transports: [
        new (winston.transports.File)({
            filename: errLoggerFileName,
            maxsize: 1048576, //1MB
            maxFiles: 2,
            json: true,
            level:'error',
            timestamp: myTimestamp,
            colorize: true,
            handleExceptions: true,
            exitOnError: false,
        })
    ],
    colorStatus: true,
    requestWhitelist: ['url', 'headers', 'method', 'httpVersion', 'originalUrl', 'query', 'body']
});

//var errorLogger = new expressWinston.logger(errLoggerObj)



var accessLogObj = {
    transports: [
        new winston.transports.File({
            filename: accessLoggerFileName,
            maxsize: 1048576, //1MB
            maxFiles: 2,
            json: true,
            timestamp: myTimestamp,
            colorize: true
        })
    ],
    meta: true, // optional: control whether you want to log the meta data about the request (default to true)
    msg: "{{res.statusCode}} {{req.method}} {{req.url}} {{res.responseTime}}ms",
    colorStatus: true, // Color the status code, using the Express/morgan color palette (default green, 3XX cyan, 4XX yellow, 5XX red).
    requestWhitelist: ['url', 'method', 'httpVersion', 'query'] /// Array of request properties to log. Overrides global requestWhitelist for this instance
}



exports.counterLogger = counterLoggerObj
exports.HTTPLogger = HTTPLoggerObj
exports.fileLogger = fileLoggerObj
exports.logger = loggerObj
exports.errorLogger = errLoggerObj
exports.accessLogger = accessLogObj