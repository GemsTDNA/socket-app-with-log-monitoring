//
var http = require('http');
var express = require('express');
var app = express();
var redis = require('redis');
var request = require('request');

var client = redis.createClient(6379, '172.16.23.27');


function getAlertsData(req, res) {
}



function sendData(req, res,next) {
    //retrieve all data from alarms

    var userName = req.body.userName;
    logger.info("the req.body from api/getGraphDetails is");
    logger.info(req.body);
    var date = new Date();
    var month = date.getMonth() + 1;
    var hourlyStatsDate = date.getDate() + '.' + month + '.' + date.getFullYear();

    request.post('http://172.16.23.27:3001/getProducts', {
            json: {
                UserName: userName
            }
        }, function(error, response, body) {
            if (error) {
              logger.error(error);
                return next(error)
            }
            else{
              if(!Array.isArray(body)){
                logger.error("get products API returns"+body);
                return next("getProducts API is down")
              }
              if(body.length ==0){
                logger.error("get products API returns an empty array"+body);
                return next("user doesn't have access to any products")
              }

            //  console.log(response)



            console.log('productList:', body); // Print the list of products the user has access to.

            var arr = [];
                // empty
            var counter = body.length;
            body.forEach(function(item) {
                    var jsonObj = {};
                    item = item.toLowerCase();
                    if (item == 'smscgw') {
                        item = 'smsgw'
                    }
                    client.hmget(item + '_counter', 'counter', function(err, data) {
                            if (err) {
                              logger.error(err);
                                return next(err)
                            }
                            //jsonObj.i[tem.total = data

                            jsonObj.product = item;
                            jsonObj.total = data[0];
                            client.hmget(item + '_critical', 'counter', function(err, criticalData) {
                                if (err) {
                                  logger.error(err);
                                    return next(err)
                                }
                                jsonObj.critical = criticalData[0];
                                client.hmget(item + '_major', 'counter', function(err, majorData) {
                                    //  counter--
                                    if (err) {
                                      logger.error(err);
                                        return next(err)
                                    }
                                    jsonObj.major = majorData[0];
                                        // smsgw_hourly_stats_24.5.2017

                                    client.hgetall(item + '_hourly_stats_' + hourlyStatsDate, function(err, hourlyData) {
                                        counter--;
                                        jsonObj.hourlyStats = [];
                                        if (err) {
                                          logger.error(err);
                                            return next(err)
                                        }

                                        if (hourlyData != null || hourlyData != undefined) {

                                            console.log(hourlyData);
                                            formatJsonData(hourlyData, function(err, data) {
                                                jsonObj.hourlyStats.push(data)
                                            })


                                            //  hourlyData.date = extractedDate
                                        }

                                        arr.push(jsonObj);

                                        if (counter == 0) {

                                            //console.log(arr)

                                            res.json({
                                                "products": arr
                                            })
                                        }
                                    })

                                })

                            })


                        }) //end of redis queries
                }) // end of foreach
              }

        }) //end of request
}


function getKPICounters(product,callback){
  var key = 'KPI';
  var objWithKPICounters = {};
  client.hmget(key+'_'+product+'_major', 'counter', function(err, majorData) {
    if(err){
      return callback(err)
    }
    if(majorData[0] == null || majorData == null){
      objWithKPICounters.majorIssues = '0'
    }
    else{
      objWithKPICounters.majorIssues = majorData[0]
    }
    client.hmget(key+'_'+product+'_critical','counter',function(err,criticalData){
      if(err){
        return callback(err)
      }
      if(criticalData[0] == null || criticalData == null){
        objWithKPICounters.criticalIssues = '0'
      }
      else{
        objWithKPICounters.criticalIssues = majorData[0]
      }
      return callback(null,objWithKPICounters)
    })

  })
}


function getControlCounters(product,callback){
  var key = 'control';
  var objWithKPICounters = {};
  client.hmget(key+'_'+product+'_major', 'counter', function(err, majorData) {
    if(err){
      return callback(err)
    }
    if(majorData[0] == null || majorData == null){
      objWithKPICounters.majorIssues = '0'
    }
    else{
      objWithKPICounters.majorIssues = majorData[0]
    }
    client.hmget(key+'_'+product+'_critical','counter',function(err,criticalData){
      if(err){
        return callback(err)
      }
      if(criticalData[0] == null || criticalData == null){
        objWithKPICounters.criticalIssues = '0'
      }
      else{
        objWithKPICounters.criticalIssues = majorData[0]
      }
      return callback(null,objWithKPICounters)
    })

  })
}



function getPerformanceCounters(product,callback){
  var key = 'performance';
  var objWithKPICounters = {};
  client.hmget(key+'_'+product+'_major', 'counter', function(err, majorData) {
    if(err){
      return callback(err)
    }
    if(majorData[0] == null || majorData == null){
      objWithKPICounters.majorIssues = '0'
    }
    else{
      objWithKPICounters.majorIssues = majorData[0]
    }
    client.hmget(key+'_'+product+'_critical','counter',function(err,criticalData){
      if(err){
        return callback(err)
      }
      if(criticalData[0] == null || criticalData == null){
        objWithKPICounters.criticalIssues = '0'
      }
      else{
        objWithKPICounters.criticalIssues = majorData[0]
      }
      return callback(null,objWithKPICounters)
    })

  })
}


function getMonitorCounters(product,callback){
  var key = 'monitor';
  var objWithKPICounters = {};
  client.hmget(key+'_'+product+'_major', 'counter', function(err, majorData) {
    if(err){
      return callback(err)
    }
    if(majorData[0] == null || majorData == null){
      objWithKPICounters.majorIssues = '0'
    }
    else{
      objWithKPICounters.majorIssues = majorData[0]
    }
    client.hmget(key+'_'+product+'_critical','counter',function(err,criticalData){
      if(err){
        return callback(err)
      }
      if(criticalData[0] == null || criticalData == null){
        objWithKPICounters.criticalIssues = '0'
      }
      else{
        objWithKPICounters.criticalIssues = majorData[0]
      }
      return callback(null,objWithKPICounters)
    })

  })
}









function formatJsonData(obj, callback) {

    var hour = [];
    var keys = Object.keys(obj);
    var numberOfKeys = keys.length;
    keys.forEach(function(key) {

        var testObj = {
            "critical": null,
            "major": null,
            "date": null
        };
        var lastDigits = key.slice(-2);
        var splitKey = key.split('_');
        var counterValueforSplitKey = obj[key];
        var date = splitKey[1];
        var counterNameOfSplitKey = splitKey[0];

        if (splitKey[0] == 'critical') {
            // check if array populates
            // yes => append to it
            testObj.critical = counterValueforSplitKey;
            testObj.date = date;
            if (hour[lastDigits] == undefined) {
                hour[lastDigits] = testObj
            } else {
                hour[lastDigits]['critical'] = counterValueforSplitKey
            }
            //hour[lastDigits] = {"major":counterValueforSplitKey,"date":date}
        }
        if (splitKey[0] == 'major') {
            // check if array populates
            // yes => append to it
            testObj.major = counterValueforSplitKey;
            testObj.date = date;
            if (hour[lastDigits] == undefined) {
                hour[lastDigits] = testObj
            } else {
                hour[lastDigits]['major'] = counterValueforSplitKey
            }
        }
        numberOfKeys--;
        if (numberOfKeys == 0) {

            var returnArray = [];
            for (i = 0; i < hour.length; i++) {
                if (hour[i] != undefined) {
                    returnArray.push(hour[i])
                }
            }
            //console.log("the return array is")
          //  console.log(returnArray)
            return callback(null, returnArray)
        }
    })


}


exports.getAlerts = getAlertsData;
exports.sendAlerts = sendData;
